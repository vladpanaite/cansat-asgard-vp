/*
 * CansatHW_Scanner
 * 
 * This utility just creates a HardwareScanner and prints diagnostic on the Serial output.
 * Only information that can be automatically discovered is shown. 
 * 
 */

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "CansatHW_Scanner.h" // Include this first to have libraries linked in the right order. 
#include "CansatConfig.h"

 void setup() {
  Serial.begin(115200);
  while (!Serial) ;

  Serial.println("Running Cansat Hardware Scanner...");
  Serial.println("(Remember the I2C bus scanning will take a *very* long time on some boards");
  Serial.println(" if pull-ups are not installed on the SCL and SDA lines)");
  Serial << F("NB: the Cansat HW Scanner detects whatever can be detected, and checks") << ENDL
         << F("    the configuration against the specification of CansatConfig.h")<< ENDL;
  Serial.flush();

  Serial << ENDL << "Initializing a CansatHW_Scanner with default values i.e values from CansatConfig.h..." << ENDL;
  CansatHW_Scanner hw;
  Serial << F("Collecting information...") << ENDL;
  hw.init();
  Serial << F("Full diagnostic:") << ENDL;
  hw.printFullDiagnostic(Serial);
}

void loop() {
  // put your main code here, to run repeatedly:

}
