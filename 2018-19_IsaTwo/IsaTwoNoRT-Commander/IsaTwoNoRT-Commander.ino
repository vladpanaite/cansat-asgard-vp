/*
   Main file of the final on-board software for CanSat IsaTw0 project.
   CSPU, 2018-19
*/

#include "IsaTwoConfig.h"
#include "IsaTwoAcquisitionProcess.h"

IsaTwoAcquisitionProcess process;

void setup() {
#ifdef INIT_SERIAL
 DINIT_IF_PIN(USB_SerialBaudRate,DbgCtrl_MasterDigitalPinNbr);
 Serial << "IsaTwo main controller without RT-Commander" << ENDL;
#endif
  process.init();
  process.startMeasurementCampaign("Test Master without RT-Commander");
  Serial<<"TMP : Forcing start campain"<< ENDL;
  DPRINTSLN(DBG_SETUP, "Setup complete");
}

void loop() {
  DPRINTSLN(DBG_LOOP, "*** Entering processing loop");
  process.run();
  DDELAY(DBG_LOOP, 500); // Slow main loop down for debugging.
  DPRINTSLN(DBG_LOOP, "*** Exiting processing loop");
}
