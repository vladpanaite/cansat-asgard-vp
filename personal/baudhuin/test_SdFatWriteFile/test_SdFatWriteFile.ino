/*
   Test writing a file on SD board (SD_Logger blocks on this on the Feather board only
*/

//#define TEST_WITHOUT_SD_LOGGER    // Define to test using the SdFat object directly.
#define TEST_WITH_SD_LOGGER    // Define to test using the SD_Logger class.
//#define TEST_WITH_LOCAL_LOGGER    // Define to test using a locally defined version of the SD_Fat class.

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_LOG_SD 1
#define DBG_INIT_HW_SCANNER 1
#define DBG_BUILD_FILE_NAME 90

constexpr byte CS_Pin = 10;

#ifdef TEST_WITHOUT_SD_LOGGER
#include "SdFat.h"
#endif

#ifdef TEST_WITH_SD_LOGGER
#include "SD_Logger.h"
SD_Logger myLogger(CS_Pin);
#endif

#ifdef TEST_WITH_LOCAL_LOGGER
#include "SdFat.h"

class LocalLogger {
  public:
    LocalLogger()  {
      logFileName.reserve(13); // To avoid heap fragmentation.
      logFileName = "noname.txt";
    };
    virtual signed char init(const char * fourCharPrefix,
                             const String& msg = "") {
      DPRINTSLN(DBG_INIT_HW_SCANNER, "Initializing Logger...");

      //      DBG_TIMER("Logger::init");

      if (!initStorage()) return -1;
      DPRINTSLN(DBG_INIT_HW_SCANNER, "Storage Init OK");

#ifdef NOTNOW_TMP
      unsigned long freeMB = freeSpaceInMBytes();
      DPRINTS(DBG_INIT_HW_SCANNER, "FreeMB: Got ");
      DPRINT(DBG_INIT_HW_SCANNER, freeMB);
      DPRINTS(DBG_INIT_HW_SCANNER, ", required ");
      DPRINTLN(DBG_INIT_HW_SCANNER, requiredFreeMegs);

      if (requiredFreeMegs > freeMB) {
        DPRINTSLN(DBG_INIT_HW_SCANNER, "Not enough space.");
        return 1;
      }
#endif
      DPRINTSLN(DBG_INIT_HW_SCANNER, "Required space OK");

      unsigned int counter = 1;
      do {
        buildFileName(fourCharPrefix, counter);
        counter++;
      } while (fileExists(logFileName.c_str()));

      DPRINTS(DBG_INIT_HW_SCANNER, "Name of the file: ");
      DPRINTLN(DBG_INIT_HW_SCANNER, logFileName); // use DPRINT and not DPRINTS to avoid using the "F" macro on a string object

      if (msg.length() > 0) {
        DPRINTS(DBG_INIT_HW_SCANNER, "writing...");
        log(msg);
        DPRINTSLN(DBG_INIT_HW_SCANNER, "written");
        DPRINTLN(DBG_INIT_HW_SCANNER, msg);
      }
      DPRINTSLN(DBG_INIT_HW_SCANNER, "logger initialized.");
      return 0;
    };
    const String& fileName() {
      return logFileName;
    };

    virtual bool log(const String& data, const bool addFinalCR = true) = 0;
  protected:
    virtual bool initStorage() = 0;
    virtual bool fileExists(const char* name) = 0;
    void buildFileName(const char* fourCharPrefix, const byte number) {

      logFileName = fourCharPrefix;
      DASSERT(logFileName.length() == 4);

      char numStr[5];
      sprintf(numStr, "%04d", number);

      logFileName += numStr;
      logFileName += ".txt";

      DPRINTS(DBG_BUILD_FILE_NAME, "Name of the file: ");
      DPRINTLN(DBG_BUILD_FILE_NAME, logFileName);
      // Do not used DPRINTSLN above to avoid wrapping the String in macro F()
    }

  private:
    String logFileName;
};

class LocalSD_Logger : public LocalLogger {
  public:
    LocalSD_Logger(byte pin) : internalSdFat(), theCS_Pin(pin) {};
    virtual bool initStorage() {
      Serial << "initStorage, pin = " << theCS_Pin << ENDL;
      return internalSdFat.begin(theCS_Pin);
    };

    virtual bool log(const String& data, const bool addFinalCR = true)  {
      bool result = false;
      DPRINTS(DBG_LOG_SD, "Logging to file ");
      DPRINTLN(DBG_LOG_SD, fileName());
      File dataFile = internalSdFat.open(fileName().c_str(), FILE_WRITE);

      if (dataFile) {
        DPRINTS(DBG_LOG_SD, "File open: data=");
        DPRINTLN(DBG_LOG_SD, data);
        if (addFinalCR) {
          dataFile.println(data.c_str());
        }
        else {
          dataFile.print(data.c_str());
        }
        DPRINTSLN(DBG_LOG_SD, "Closing");
        dataFile.close();

        DPRINTS(DBG_LOG_SD, "data written: ");
        DPRINTLN(DBG_LOG_SD, data);
        result = true;
      }
    }
  protected:
    bool fileExists(const char* name)
    {
      return internalSdFat.exists(name);
    }
  private:
    SdFat internalSdFat;
    byte theCS_Pin;
};

LocalSD_Logger myLogger(CS_Pin);

#endif

void setup() {
  DINIT(115200);
  Serial << "Serial OK" << ENDL;

  pinMode(CS_Pin, OUTPUT);
  SPI.begin();
#ifdef TEST_WITHOUT_SD_LOGGER
  {
    SdFat mySD;
    bool result = mySD.begin(CS_Pin);
    if (result)
    {
      Serial << " SdFat.begin OK" << ENDL;
      String fileName("myText.txt");
      String data("testABDC");

      File dataFile = mySD.open(fileName, FILE_WRITE);

      if (dataFile) {
        dataFile.println(data);
        dataFile.close();

        Serial << "data written " << ENDL;
      }
      else Serial << "DataFile NOK" << ENDL;
    }
    else Serial << "SdFat.begin NOK" << ENDL;
  }
#endif

#ifdef TEST_WITH_SD_LOGGER
  signed char result = myLogger.init("dcba", "first msg");
  if (result == 0)
  {
    if (myLogger.log("second line")) {
      Serial << "log OK " << ENDL;
    }
    else Serial << "log NOK" << ENDL;
  }
  else Serial << "logger init NOK" << ENDL;

#endif

#ifdef TEST_WITH_LOCAL_LOGGER
  signed char result = myLogger.init("dcba", "first msg");
  if (result == 0)
  {
    if (myLogger.log("second line")) {
      Serial << "local log OK " << ENDL;
    }
    else Serial << "local log NOK" << ENDL;
  }
  else Serial << "local  logger init NOK" << ENDL;

#endif
}

void loop() {
  // put your main code here, to run repeatedly:

}
