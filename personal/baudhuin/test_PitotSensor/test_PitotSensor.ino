//#include <Arduino.h>
#include "SDP.h"
#include "Wire.h"

SDP_Controller SDP800;

float DP, TMP;
uint32_t prevMillis = 0;

void setup(){

  while(!Serial){
    Serial.begin(115200); //If serial is available 
  }
    Serial.println("Initializing...........................................");
    delay(4000);

  Wire.begin();
  SDP800.begin();
  SDP800.startContinuousMeasurement(SDP_TEMPCOMP_DIFFERENTIAL_PRESSURE, SDP_AVERAGING_TILL_READ);
}

void loop(){
  // reading at 1Hz
  if((millis() - prevMillis) > 1000){
    // Trigger measurement with differential pressure temperature compensation and without clock stretching
    DP = SDP800.getDiffPressureTrigger(SDP_TEMPCOMP_DIFFERENTIAL_PRESSURE, SDP_CLKST_NONE);
    TMP = SDP800.getTemperature();
    Serial.println(DP);
    Serial.println(TMP);
  }

}
