// Silence warnings in standard arduino files
#if defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS) || defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_MICRO)
 /* v1.1.8 of the Arduino Servo library does not support the SAMD51 processor.
  * Switch to Adafruit version to support SAMD51 https://github.com/adafruit/?q=servo
  * Questions: does it support AVR as well ? */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "ServoWinch.h"

#define DEBUG_CSPU
#include "DebugCSPU.h"

#define DBG 1
#define DBG_SERVOINIT 0
#define DBG_SERVOSET 0
#define DBG_SERVOEND 1
#define DBG_DIAGNOSTIC 1

ServoWinch::ServoWinch(uint16_t maxRopeLenToUse, uint16_t minPulseWidthToUse, uint16_t maxPulseWidthToUse) : maxRopeLen(maxRopeLenToUse), minPulseWidth(minPulseWidthToUse), maxPulseWidth(maxPulseWidthToUse) {}

bool ServoWinch::begin(byte PWM_Pin, ValueType initPositionType, uint16_t initPosition) {
  if (running) {
    DPRINTLN(DBG_DIAGNOSTIC, "Can't call ServoWinch::begin because servo is already on.");
    return false;
  }

  DPRINTLN(DBG_SERVOINIT, "Attaching servo-motor.");

  servo.attach(PWM_Pin, minPulseWidth, maxPulseWidth);
  running = true;
  return set(initPositionType, initPosition);
}

bool ServoWinch::end() {
  if (!running) {
    DPRINTLN(DBG_DIAGNOSTIC, "Can't call ServoWinch::end because servo is not on.");
    return false;
  }

  servo.detach();
  running = false;

  DPRINTLN(DBG_SERVOEND, "Motor powered off.");

  return true;
}

bool ServoWinch::set(ValueType type, uint16_t value) {
  if (!running) {
    DPRINTLN(DBG_DIAGNOSTIC, "Can't call ServoWinch::set because servo is not on.");
    return false;
  }

  switch (type) {
    case ValueType::pulseWidth:
      if (value < minPulseWidth || maxPulseWidth < value) {
        DPRINTLN(DBG_DIAGNOSTIC, "Value provided to ServoWinch::set is out of bounds. Ignoring command.");
        return false;
      }
      currPulseWidth = value;
      currRopeLen = getRopeLenFromPulseWidth(value);
      break;

    case ValueType::ropeLength:
    	if (maxRopeLen < value) {
        DPRINTLN(DBG_DIAGNOSTIC, "Value provided to ServoWinch::set is out of bounds. Ignoring command.");
        return false;
      }
      currPulseWidth = getPulseWidthFromRopeLen(value);
      currRopeLen = value;
      break;

    default:
      DPRINTLN(DBG, "Unexpected ValueType value. Terminating program.");
      DASSERT (false);
      return false;
  }

  DPRINT(DBG_SERVOSET, "Setting servo-motor pulse width to "); DPRINTLN(DBG_SERVOSET, currPulseWidth);
  servo.writeMicroseconds(currPulseWidth);

  return true;
}

uint16_t ServoWinch::getCurrRopeLen() const {
  return currRopeLen;
}

uint16_t ServoWinch::getCurrPulseWidth() const {
  return currPulseWidth;
}

uint16_t ServoWinch::getRopeLenFromPulseWidth(uint16_t pulseWidth) const {
  DASSERT(minPulseWidth <= pulseWidth && pulseWidth <= maxPulseWidth);

  return map(pulseWidth, minPulseWidth, maxPulseWidth, 0, maxRopeLen);
}

uint16_t ServoWinch::getPulseWidthFromRopeLen(uint16_t ropeLen) const {
  DASSERT(ropeLen <= maxRopeLen);

  return map(ropeLen, 0, maxRopeLen, minPulseWidth, maxPulseWidth);
}
#endif // processor.
