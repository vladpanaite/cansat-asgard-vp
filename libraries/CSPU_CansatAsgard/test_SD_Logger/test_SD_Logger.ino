/*
    Test for SD_Logger

    Tested here:
      Test 1: Initializing logger with no message.
      Test 2: Initializing logger with no message, and request 100 MB
      Test 3: Initializing logger with no message, and request too much free space (100000 MB)
      Test 4: Initialize logger, and write something in 1 line (no init message)
      Test 5: Initialize logger, and write something in multiple lines (no init message)
      Test 6: Initialize logger, and write something in multiple lines (with init message)
      Test 7: Initialize logger, request 100 MB, and write something in multiple lines (with init message)
      Test 8: Initialize logger with extension other than \"txt\", and write something in multiple lines (with init message)
      Test 9: performing maintenance
      Test 10: testing duration of logging
      Test 11: testing duration between maintenance
    
    Other tests:
      SpySeeCanCSPU/test_SD_Logger
      cansatAsgardCSPU/test_SD-Flash_Logger
    
    Wiring:
      Uno: CS to D4, DI (MOSI) = D11, DO (MISO) = D12, SCK = 13, GND to GND, 5V to 5V
      Feather M0 Express: CS to 10, DI to MO (MOSI), DO to MI (MISO), CLK to SCK, GND to GND, 3V to 3V.
      Feather M0 Adalogger: no wiring required, CS is 4 (internal pin).

    Expected results:
      If everyting is correctly wired and SD card is inserted: all test should succeed
      If the device is not connected or the card is not inserted: tests 9 and 11 should succeed
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop


#include "SD_Logger.h"
#include "elapsedMillis.h"

const byte CS_Pin = 4; // change to 10 if using Feather M0 express

SD_Logger theLogger(CS_Pin);


#define USE_ASSERTIONS
#define DEBUG_CSPU

#include "DebugCSPU.h"


int succeededTests = 0;

//Functions used in testing:

String concatenate(String s, long unsigned n) {
  String result = "";
  for (unsigned int i = 0; i < n; i++) {
    result += s;
  }
  return result;
}

int logAverageTime(String msg, long unsigned n) {
  elapsedMillis elapsed = 0;
  bool logresult;
  float averageDuration = 0;
  float currDuration = 0;
  for (unsigned int i = 1; i <= n; i++) {
    elapsed = 0;
    logresult = theLogger.log(msg);
    if (!logresult) {
      Serial << "Logging failed. \n Test failed. \n";
      return -1;
    }
    currDuration = elapsed;
    averageDuration = ((i - 1) * averageDuration + currDuration) / i;
  }
  return (int)averageDuration;
}




//Different tests:


void test1to8(String initMsg, String messages[], int numMessages, bool multipleLines, unsigned long requestedFreeMegs, const char * extension = "txt", bool tooMuchSpaceRequested = false) {
  Serial << "Initialising logger..." << ENDL;

  int initMsgLen  = (initMsg.length() == 0 ? 0 : initMsg.length() + 2); // if there is an init message, an endline will be added causing the file size to increase by 2 bytes
  if (initMsgLen != 0) {
    Serial << "Init message: " << initMsg << ENDL;
    Serial << "Init message length = " << initMsgLen << ENDL;
  }
  if (requestedFreeMegs > 0) {
    Serial << "Requesting " << requestedFreeMegs << " MB free on the card" << ENDL;
  }

  auto initresult = theLogger.init( initMsg.c_str(),"test", requestedFreeMegs, extension);
  if (initresult == 1 && tooMuchSpaceRequested) { // in case too much space was volontairly requested, test passed
    Serial << "Required free space not available." << ENDL;
    Serial << "End of test: all OK" << ENDL;
    succeededTests++;
    return;
  }
  else if (initresult != 0) { // in case init failed
    Serial << "Init failed.";
    Serial << "Test failed." << ENDL; return;
  } 
  Serial << "Init OK." << ENDL;

  Serial << "File name: " << theLogger.fileName() << ENDL;
  int fileSize = theLogger.fileSize();
  int expectedFileSize = initMsgLen;
  Serial << "File size (bytes) = " << fileSize << ENDL;
  Serial << "Expected file size (bytes) = " << expectedFileSize << ENDL;
  if (fileSize != expectedFileSize) {
    Serial << "File size is not as expected." << ENDL;
    Serial << "Test failed." << ENDL; return;
  }

  if (numMessages != 0) {
    Serial << "About to log a few messages" << ENDL;
    for (int i = 0; i < numMessages; i++) {
      String msg = messages[i];
      expectedFileSize += msg.length();
      expectedFileSize += 2 * ((int)multipleLines); //because endline is 2 characters
      Serial << "Logging message " << (multipleLines ? "with endline. " : "without endline. ") << "Content: " << msg << ENDL;
      bool logresult = theLogger.log(msg, multipleLines);
      if (!logresult) {
        Serial << "Logging failed. \n Test failed. \n";
        return;
      }
      fileSize = theLogger.fileSize();
      Serial << "File size (bytes) = " << fileSize << ENDL;
      Serial << "Expected file size (bytes) = " << expectedFileSize << ENDL;
      if (fileSize != expectedFileSize) {
        Serial << "File size is not as expected." << ENDL;
        Serial << "Test failed." << ENDL; return;
      }
    }
  }


  Serial << "End of test: all OK" << ENDL;
  succeededTests++;
}



void test9() {
  Serial << "Trying to perform maintenance..." << ENDL;
  elapsedMillis elapsed = 0;
  while (!theLogger.doIdle()) {
    if (elapsed > 20000) {
      Serial << "Timeout: waited to long for maintenance (should be performed every 10 seconds, but already waiting 20 seconds)" << ENDL;
      Serial << "Test failed." << ENDL;
      return;
    }
  }
  Serial << "Maintenance succeeded" << ENDL;
  Serial << "End of test: all OK" << ENDL;
  succeededTests++;
}


void test10() {
  auto initresult = theLogger.init("test");
  if (initresult != 0) {
    Serial << "Init failed. Test cannot be performed. Ending test" << ENDL;
    return;
  }
  Serial << "Name of file used for this test: " << theLogger.fileName() << ENDL;
  Serial << "Testing duration of logging:" << ENDL;

  int averageDuration;

  String msg = "a";
  averageDuration = logAverageTime(msg, 100);
  if (averageDuration == -1) return;
  if (averageDuration > 100) {
    Serial << "Too long average duration: test failed." << ENDL;
    return;
  }
  Serial << "1 byte: average of " << averageDuration << " msec" << ENDL;

  msg = concatenate("a", 10);
  averageDuration = logAverageTime(msg, 100);
  if (averageDuration == -1) return;
  if (averageDuration > 100) {
    Serial << "Too long average duration: test failed." << ENDL;
    return;
  }
  Serial << "10 bytes: average of " << averageDuration << " msec" << ENDL;

  msg = concatenate("a", 100);
  averageDuration = logAverageTime(msg, 100);
  if (averageDuration == -1) return;
  if (averageDuration > 100) {
    Serial << "Too long average duration: test failed." << ENDL;
    return;
  }
  Serial << "100 bytes: average of " << averageDuration << " msec" << ENDL;

  msg = concatenate("a", 1024);
  averageDuration = logAverageTime(msg, 100);
  if (averageDuration == -1) return;
  if (averageDuration > 100) {
    Serial << "Too long average duration: test failed." << ENDL;
    return;
  }
  Serial << "1 KB: average of " << averageDuration << " msec" << ENDL;

  Serial << "End of test: all OK" << ENDL;
  succeededTests++;
}


void test11() {
  Serial << "Expected duration beteween maintenances: 10000 msec" << ENDL;
  elapsedMillis elapsed = 0;
  while (!theLogger.doIdle()) {
    if (elapsed > 20000) {
      Serial << "Timeout: waited to long for maintenance (should be performed every 10 seconds, but already waiting 20 seconds)" << ENDL;
      Serial << "Test failed." << ENDL;
      return;
    }
  }
  Serial << "First maintenance performed. Waiting for second one." << ENDL;
  elapsed = 0;
  while (!theLogger.doIdle()) {
    if (elapsed > 20000) {
      Serial << "Timeout: waited to long for maintenance (should be performed every 10 seconds, but already waiting 20 seconds)" << ENDL;
      Serial << "Test failed." << ENDL;
      return;
    }
  }
  Serial << "Second maintenance performed." << ENDL;
  Serial << "Time measured between 2 maintenances: " << elapsed << " msec" << ENDL;
  if (elapsed > 15000) {
    Serial << "Time between 2 maintenances was too long. Test failed." << ENDL;
    return;
  }
  if (elapsed < 5000) {
    Serial << "Time between 2 maintenances was too short. Test failed." << ENDL;
    return;
  }

  Serial << "End of test: all OK" << ENDL;
  succeededTests++;
}

void setup() {
  DINIT(115200);
  pinMode(CS_Pin, OUTPUT);
  SPI.begin();


  Serial << "Begin of tests." << ENDL << ENDL;

  String nothing[] = {};

  Serial << "Test 1: Initializing logger with no message." << ENDL;
  test1to8("", nothing, 0, false, 0);
  Serial << ENDL;

  Serial << "Test 2: Initializing logger with no message, and request 100 MB" << ENDL;
  test1to8("", nothing, 0, false, 100);
  Serial << ENDL;

  Serial << "Test 3: Initializing logger with no message, and request too much free space (100000 MB)" << ENDL;
  test1to8("", nothing, 0, false, 100000, "txt", true);
  Serial << ENDL;

  const int numMsg = 3;
  String messages[numMsg] = {"msg1", "msg2", "msg3"};

  Serial << "Test 4: Initialize logger, and write something in 1 line (no init message)" << ENDL;
  test1to8("", messages, numMsg, false, 0);
  Serial << ENDL;

  Serial << "Test 5: Initialize logger, and write something in multiple lines (no init message)" << ENDL;
  test1to8("", messages, numMsg, true, 0);
  Serial << ENDL;

  Serial << "Test 6: Initialize logger, and write something in multiple lines (with init message)" << ENDL;
  test1to8("init msg", messages, numMsg, true, 0);
  Serial << ENDL;
  
  Serial << "Test 7: Initialize logger, request 100 MB, and write something in multiple lines (with init message)" << ENDL;
  test1to8("init msg", messages, numMsg, true, 100);
  Serial << ENDL;

  Serial << "Test 8: Initialize logger with extension other than \"txt\", and write something in multiple lines (with init message)" << ENDL;
  test1to8("init msg", messages, numMsg, true, 0, "abc");
  Serial << ENDL;

  Serial << "Test 9: performing maintenance" << ENDL;
  test9();
  Serial << ENDL;

  Serial << "Test 10: testing duration of logging" << ENDL;
  test10();
  Serial << ENDL;

  Serial << "Test 11: testing duration between maintenance" << ENDL;
  test11();
  Serial << ENDL;

  Serial << "End of all tests." << ENDL << "Passed: " << succeededTests << "/" << "11" << ENDL;

}

void loop() {
  ;
}
