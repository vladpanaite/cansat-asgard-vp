/*
   DoubleSerial.cpp
*/
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "DoubleSerial.h"
#include <stdlib.h>
#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_SP 0

DoubleSerial::DoubleSerial(const uint16_t theSelectionCode)
  : selectionCode(theSelectionCode), serial2(NULL) {}

void DoubleSerial::begin (Stream& theSerial2) {
  serial2 = &theSerial2;
}

DoubleSerial& DoubleSerial::operator<<(const char * line) {
  DASSERT(serial2);
  DPRINT(DBG_SP, "Line : ");
  DPRINTLN(DBG_SP, line);
  char* nextCharAddress;
  long int code = strtol(line, &nextCharAddress, 10);
  DPRINT(DBG_SP, "Code : ");
  DPRINTLN(DBG_SP, code);
  DPRINT(DBG_SP, "Next char : ");
  DPRINTLN(DBG_SP, *nextCharAddress);
  if (code == selectionCode) {
    while (*nextCharAddress == ' ') {
      nextCharAddress++;
    }
    if (*nextCharAddress == ',') {
      serial2->println(line);
    }
    else {
      Serial.println(line);
    }
  }
  else {
    Serial.println(line);
  }
  return *this;
}

void DoubleSerial::flush() {
  DASSERT(serial2);
  Serial.flush();
  serial2->flush();
}
