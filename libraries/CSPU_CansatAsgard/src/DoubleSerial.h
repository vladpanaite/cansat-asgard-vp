/*
 * DoubleSerial.h
 * 
 */
#pragma once

#include <Arduino.h>

/** @ingroup CSPU_CansatAsgard
 *  @brief This class splits a data flow over two different serial ports.
 *  Routing is performed line by line, as follows:
 *    - If the first characters of the line are a well-formed integer, followed
 *      by a comma, this number is compared to a selection code.
 *      If equal to the selection code, the line is forwarded to the second serial port
 *      else it is forwarded to the default Serial object.
 *    - Else the line is forwarded to the default Serial object.
 */
class DoubleSerial {
  public:
    /**
     *  @param selectionCode The number used to select the port to send the data
     *         to. See class documentation for the routing logic.
     *
     */
      DoubleSerial(const uint16_t selectionCode);
    /**
     * @brief Initialization of the object (call before any use).
     * @param theSerial2 The stream object managing the serial port to the second device.
     * 		  The caller retains the owner ship and the object should exist as long as the
     * 		  DoubleSerial instance that uses it.
     * @pre The standard Serial object and theSerial2 are assumed to be properly initialized.
     */
     void begin(Stream& theSerial2);
     
     /**
      * @brief Stream the data to one of the serial ports
      * See class documentation for the routing logic.
      * @param str The string to process.
      * @pre begin() has been called successfully.
      */
    DoubleSerial& operator<<(const char * str);
    
    /** @brief This function is used to print the data possibly buffered since
     *  the last calls to the streaming operator. */
    void flush();

  private:
    uint16_t selectionCode; /*!< The value triggering the routing to the second serial port. */
    Stream* serial2;  /*!< Pointer to the object managing the second serial port. */
};
