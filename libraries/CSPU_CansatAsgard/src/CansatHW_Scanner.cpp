/*
 * CansatHW_Scanner.cpp
 *
 *  Created on: 15 Oct 2020
 *      Author: Alain
 */
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"

#include "CansatHW_Scanner.h"
#include "SdFat.h"
#ifdef ARDUINO_ARCH_SAMD
#  include "Serial2.h"
#endif
#include "CansatConfig.h"

#define DBG_CHECK_SPI_DEVICES 0
// DBG_DIAGNOSTIC, DBG_INIT are defined in CansatConfig.h

CansatHW_Scanner::CansatHW_Scanner(const byte anUnusedAnalogInPin) :
		HardwareScanner(anUnusedAnalogInPin),
		mSD_CardChipSelect(0),
		mRF_SerialPortNumber(0),
		mGPS_SerialPortNumber(0),
#ifdef RF_ACTIVATE_API_MODE
		xbClient(GroundXBeeAddressSH,GroundXBeeAddressSL),
#endif
		RF_Serial(NULL)
{
	flags.SD_CardReaderAvailable = false;
	flags.BMP_SensorAvailable = false;
	flags.RF_InterfaceAvailable = false;
	flags.GPS_InterfaceAvailable = false;
	flags.XBeeClientAvailable = false;
} ;

void CansatHW_Scanner::init(  const byte firstI2C_Address,
                        	  const byte lastI2C_Address,
							  const uint16_t wireLibBitRateInKhz)
{
	init(	SD_CardChipSelect, firstI2C_Address, lastI2C_Address,
			RF_SerialPortNumber, GPS_SerialPortNumber, wireLibBitRateInKhz);
}

void CansatHW_Scanner::init(
  const byte SD_CardCS,
  const byte firstI2C_Address,
  const byte lastI2C_Address,
  const byte RF_SerialPort,
  const byte GPS_SerialPort,
  const uint16_t bitRateInKhz)
{
	DPRINTSLN(DBG_INIT, "CansatHW_Scanner::init");

	mSD_CardChipSelect=SD_CardCS;
	HardwareScanner::init(firstI2C_Address, lastI2C_Address, bitRateInKhz);
	checkProjectI2C_Devices();
	flags.BMP_SensorAvailable = isI2C_SlaveUpAndRunning(I2C_BMP_SensorAddress);

	// Check/initialize RF
	DPRINTSLN(DBG_INIT, "Checking RF...");
	if (RF_SerialPort) {
		DPRINTSLN(DBG_INIT, "Looking for RF Serial port...");
		mRF_SerialPortNumber=RF_SerialPort;
		RF_Serial = getSerialObject(RF_SerialPort);
		// We cannot use RF_Serial for initialisation;
		// Because on SAMD boards, it cannot possibly return an HardwareSerial*,
		// and the next common base class to all ports is Stream, which does
		// not have a begin() method.
		HardwareSerial* localRF_SerialPort=NULL;
		if (isSerialPortAvailable(RF_SerialPort)) {
			switch (RF_SerialPort) {
			case 1:
#if defined(UBRR1H) || defined (HAVE_HWSERIAL1)
				localRF_SerialPort = &Serial1;
#endif
				break;
			case 2:
#if defined(UBRR2H) || defined (HAVE_HWSERIAL2)
				localRF_SerialPort = &Serial2;
#endif
				break;
			case 3:
#if defined(UBRR3H)|| defined (HAVE_HWSERIAL3)
				localRF_SerialPort = &Serial3;
#endif
				break;
			default:
				DPRINTS(DBG_DIAGNOSTIC, "Cannot find RF Serial port (");
				DPRINT(DBG_DIAGNOSTIC, RF_SerialPort);
				DPRINTSLN(DBG_DIAGNOSTIC, ")");
				DASSERT(false)
			} // switch
		} // if RF_SerialPort available

		if (localRF_SerialPort) {
			DPRINTS(DBG_INIT, "Initialising RF Serial port...");
			localRF_SerialPort->begin(RF_SerialBaudRate);
			while (!RF_Serial) {
				;
			}
			flags.RF_InterfaceAvailable = true;
			DPRINTSLN(DBG_INIT, "OK");
			DPRINTS(DBG_INIT, "RF Serial init ok at ");
			DPRINT(DBG_INIT, RF_SerialBaudRate);
			DPRINTSLN(DBG_INIT, " bauds");
#ifdef RF_ACTIVATE_API_MODE
			xbClient.begin(*RF_Serial); // Cannot fail
			flags.XBeeClientAvailable = true;
#endif
		} else {
			DPRINTSLN(DBG_DIAGNOSTIC, "*** Could no find serial object for RF");
		}
	} else DPRINTSLN(DBG_DIAGNOSTIC, "*** No RF Serial port");

	// Check GPS. NB: Do not initialize the port: this is taken care of by the GPS_Client.
	if (GPS_SerialPort) {
		mGPS_SerialPortNumber = GPS_SerialPort;
		if (!isSerialPortAvailable(GPS_SerialPortNumber)) {
			DPRINTSLN(DBG_DIAGNOSTIC, "*** No GPS Serial port");
		} else {
			DPRINTSLN(DBG_INIT, "GPS Serial port available");
			flags.GPS_InterfaceAvailable = true;
		}
	} //GPS_SerialPort.
}

void CansatHW_Scanner::checkAllSPI_Devices() {
	// Check for standard devices
	DPRINTSLN(DBG_CHECK_SPI_DEVICES, "HardwareScanner::checkAllSPI_Devices");
	if (mSD_CardChipSelect) {
		// Check for SD Card reader
		SdFat SD;
		pinMode(mSD_CardChipSelect, OUTPUT);
		if (SD.begin(mSD_CardChipSelect)) {
			flags.SD_CardReaderAvailable = true;
		} else {
			flags.SD_CardReaderAvailable = false;
			DPRINTSLN(DBG_CHECK_SPI_DEVICES, "Card failed, or not present");
		}
	}

	// Check for project-specific ones
	checkProjectSPI_Devices();
}

void CansatHW_Scanner::printFullDiagnostic(Stream& stream) const {
	HardwareScanner::printFullDiagnostic(stream);

	if (flags.RF_InterfaceAvailable) {
		stream << F("RF assumed on Serial") << mRF_SerialPortNumber << F(" (")
				<< RF_SerialBaudRate << F(" bauds)") << ENDL;
		if (mRF_SerialPortNumber == 2) {
			stream << F("    TX=10, RX = 11") << ENDL;
		}
#ifdef RF_ACTIVATE_API_MODE
		if (flags.XBeeClientAvailable) {
			stream << F("Using RF API mode. XBee client initialized.") << ENDL;
		} else {
			stream
					<< F(
							"*** RF API mode requested, but XBee client not initialized.")
					<< ENDL;
		}
		stream << F("      XBee set '") << RF_XBEE_MODULES_SET
				<< F("', Receiver (ground) addr: 0x");
		stream.print(GroundXBeeAddressSH, HEX);
		stream << "-0x";
		stream.println(GroundXBeeAddressSL, HEX);
#else
		stream << F("Using RF TRANSPARENT mode") << ENDL;
#endif
	} else
		stream << F("No RF serial port (") << mRF_SerialPortNumber << ")"
				<< ENDL;

	if (flags.GPS_InterfaceAvailable) {
		stream << F("GPS assumed on Serial") << mGPS_SerialPortNumber << ENDL;
	} else
		stream << F("No GPS serial port (") << mGPS_SerialPortNumber << ")"
				<< ENDL;
	delay(300); // Avoid overflowing the stream buffer, in case
				// it is slow (e.g. a radio transmitter)...

	stream << F("Thermistor(s) input on analog pin(s) A")
			<< Thermistor1_AnalogInPinNbr - A0 << F(", A")
			<< Thermistor2_AnalogInPinNbr - A0 << F(", A")
			<< Thermistor3_AnalogInPinNbr - A0 << ENDL;
	printProjectDiagnostic(stream);
}

void CansatHW_Scanner::printI2C_Diagnostic(Stream& stream) const {
	// Generic detection
	HardwareScanner::printI2C_Diagnostic(stream);
	// information about standard Cansat sensors
	if (flags.BMP_SensorAvailable) {
		stream << F("Assuming I2C slave at ") << I2C_BMP_SensorAddress
				<< F(" is the BMP sensor") << ENDL;
	} else {
		stream << F("*** Missing BMP sensor at I2C address ")
				<< I2C_BMP_SensorAddress << ENDL;
	}
	// Project-specific information
	printProjectI2C_Diagnostic(stream);
}

void CansatHW_Scanner::printSPI_Diagnostic(Stream& stream) const {
	stream.print("SPI bus: ");
	if (mSD_CardChipSelect) {
		if (flags.SD_CardReaderAvailable) {
			stream.print(F("SD Card ok. CS="));
		} else {
			stream.print(F("*** No SD Card detected. *** CS="));
		}
		stream.println(mSD_CardChipSelect);
		delay(100);
	} else stream.println();
	printProjectSPI_Diagnostic(stream);
}

byte CansatHW_Scanner::getLED_PinNbr(LED_Type type) {
	byte result;
	  switch (type) {
	    case LED_Type::Init:
	      result = pin_InitializationLED;
	      break;
	    case LED_Type::Storage:
	      result =  pin_StorageLED;
	      break;
	    case LED_Type::Transmission:
	      result =  pin_TransmissionLED;
	      break;
	    case LED_Type::Acquisition:
	      result =  pin_AcquisitionLED;
	      break;
	    case LED_Type::Heartbeat:
	      result =  pin_HeartbeatLED;
	      break;
	    case LED_Type::Campaign:
	      result =  pin_CampaignLED;
	      break;
	    case LED_Type::UsingEEPROM:
	      result =  pin_UsingEEPROM_LED;
	      break;
	    default:
	      DASSERT(false);
	      result =  0;
	  }
	  return result;
}

