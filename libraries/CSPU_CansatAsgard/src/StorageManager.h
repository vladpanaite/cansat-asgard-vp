/*
    StorageManager.h
*/

#pragma once
#include "HardwareScanner.h"
#include "SD_Logger.h"
#include "EEPROM_BankWriter.h"
#include "CansatConfig.h"
#include "CansatRecord.h"

/** @ingroup CSPU_CansatAsgard
 *  @brief A class implementing all storage functions common to any Arduino project.
 *  It receives CansatRecords or strings and stores them to SD-Card and or to EEPROM, according
 *  to the available storage, and anticipated duration of the mission and the actual
 *  presence of EEPROMS in the can.
 *  
 *  @note Although first version of this class required subclass by each project, this version
 *  should be operational with any subclass of CansatRecord.
 *
 *  Still to be implemented (not required todate):
 *  	- use of the EEPROM memory depending on the duration of the mission.
 *
 *  NB: symbol IGNORE_EEPROMS (defined or not in file CansatConfig.h) strips all eeprom-
 *      related functions.
 *  TO IMPROVE: This class should maintain a Logger, not a SD_Logger, in order to support
 *              the use of a FlashLogger as well.
 */
class StorageManager {
  public:
    /** Constructor.
     *  @param theCampainDurationInSec The duration of the measurement campaign in seconds, in order for
     *         the storage manager to plan for the usage of th various storage capabilities.
     *  @param theMinStoragePeriodInMsec The minimum time interval between the storage of two records (in msec).
     *         If records are provided before this delay expired, the record will not be stored.
     */
    StorageManager(unsigned int /* theCampainDurationInSec */, unsigned int /* theMinStoragePeriodInMsec */);
    NON_VIRTUAL ~StorageManager() {};

    /** Prepare the StorageManager for work. Call once (in setup()) before calling storeOneRecord().
       If implemented in a subclass, be sure to call inherited::init() before performing additional initialization.
       Default values for parameters are defined in CansatConfig.h

       @param recordTemplate An instance of the CansatRecord subclass which will be stored
       	   	   The record can be empty.
        @pre SPI port is initialized (this class does not call SPI.begin();
        @param loggerFirstLine  If not an empty string, it is written as first line of the file. The file is then closed.
        @param fourCharPrefix The prefix used in the file name
        @param chipSelectPinNumber The pin used as CS for the SD-Card reader.
        @param requiredFreeMegs The number of free Mbytes to be available on the storage.
        						If 0, no check is performed on available space.
		@param extension Extension of the file that will be used. Must be maximum 3 characters long.
							Default value: "txt"

       @return true if the init was successful, false otherwise. This init is considered successful if at least one
       storage destination was successfully initialized.
    */
    NON_VIRTUAL bool init(CansatRecord& recordTemplate,
    				  HardwareScanner* hwScanner = NULL,
					  const String& loggerFirstLine="",
                      const char * fourCharPrefix = SD_FilesPrefix,
                      const byte chipSelectPinNumber = SD_CardChipSelect,
                      const unsigned int requiredFreeMegs = SD_RequiredFreeMegs,
                      const EEPROM_BankWriter::EEPROM_Key key = EEPROM_KeyValue);

    /** Store a record of data. The size is expected to match the size defined when calling init()
       @param record The record to store.
       @param useEEPROM Store in EEPROM as well (if any)?
    */
    NON_VIRTUAL void storeOneRecord(const CansatRecord & record, const bool useEEPROM=true);

    /** Store a string on the SD-Card.
     *  @param stringData The string to store.
     */
    NON_VIRTUAL void storeString(const String& stringData);

    /** Store a string on the SD-Card.
     *  @param str The string to store (\0 terminated)..
     */
    NON_VIRTUAL void storeString(const char* str);

    /** Get number of records that can be stored in EEPROM
    @return The number of record that can still be accommodated in EEPROM. */
    NON_VIRTUAL unsigned long getNumFreeEEEPROM_Records() const; 
    /** Check whether the logger is operational.
     *  @return true if operational, false otherwise.
     */
    NON_VIRTUAL bool LoggerOperational() const;
    
    /** Call this method regularly to allow for internal maintenance. */ 
    NON_VIRTUAL void doIdle();

    /** Obtain a pointer to the SdFat object (fully initialized and ready to use) if any
     *  @return Pointer to the SdFat object, or null if none available.
     */
     NON_VIRTUAL SdFat* getSdFat(){ return logger.getSdFat();};

     NON_VIRTUAL const String& getSD_FileName() const {return logger.fileName();};
  private:
    /*Actually perform the storage of the record. */

    uint8_t recordSize;        		/**< The size of the record in binary format */
    unsigned int campainDuration;   /**< The expected duration of the measurement campain, in seconds. */
    unsigned int minStoragePeriod;  /**< The minimum delay between 2 storage requests in msec. */
    bool initLogger;                /**< True if SD card was succesfully initialized. */
    bool initEEPROM;                /**< True if EEPROM_Bank was successfully initialized */
#ifndef IGNORE_EEPROMS
    EEPROM_BankWriter eeprom;       /**< The interface object to the EEPROM */
#endif
    SD_Logger logger;               /**< The logger */
};
