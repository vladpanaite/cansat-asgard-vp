/**
 * test_onBoardCanCommander.ino
 *
 * This is the on-board CanCommander, used when testing with RF in API mode.
 * Run this test on a Feather M0 Express board only.
 * See header of test_RT_CanCommander.ino for details.
 * */
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

constexpr byte SD_CS=4;    // CS for SD-Card reader. If using an Adalogger Feather M0 Express board,
						   // the onboard SD reader is on pin 4 (which is not broken out).
constexpr byte LED_PinNbr=LED_BUILTIN;// pin connected to LED. LED_BUILTIN is the on-boar one.

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG 1
#include "CansatConfig.h"
#include "CansatXBeeClient.h"
#include "../test_RT_CanCommander/test_RT_CanCommander/ProjectRT_CanCommander.h"  // relative to src!
#include "CansatRecordExample.h"
#include "SdFat.h"
#include "SPI.h"

#ifndef RF_ACTIVATE_API_MODE
#  error "This program is only usefull if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif
#ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#  error "This program only works on Feather MO_Express"
#endif

HardwareSerial &RF = Serial1;
CansatXBeeClient xbc(GroundXBeeAddressSH, GroundXBeeAddressSL); // Defined in CansatConfig.h
CansatRecordExample myRecord;
char myString[XBeeClient::MaxStringSize +10];
CansatFrameType stringType;
bool isRecord;
SdFat sd;
ProjectRT_CanCommander cmd(10000);

void setup() {
	  DINIT(115200);
	  pinMode(LED_PinNbr, OUTPUT);
	  digitalWrite(LED_PinNbr, LOW);
	  Serial << "RT-CanCommander unit test (simulating the can)" << ENDL;
	  Serial << "Waiting for commands on the RF interface (run test_RT_CanCommander as ground simulator)" <<ENDL;
	  Serial << "  SD-Card CS = " << SD_CS << ENDL;
	  Serial << "  LED is on pin #" << LED_PinNbr << ENDL;
	  SPI.begin();
	  if (!sd.begin(SD_CS)) {
	    sd.initErrorHalt();
	  }
	  Serial << "  SD-Card OK" << ENDL;
	  if (!sd.exists("Folder1")) {                              /**< Tests the existance of Folder1*/
	    Serial << F("Test Folder1 cannot be found. Please run the ""CreateTestFiles.ino"" sketch first") << ENDL;
	    while(1) delay(100);
	  }
	  if (!sd.exists("Folder2")) {                              /**< Tests the existance of Folder2*/
	    Serial << F("Test Folder2 cannot be found. Please run the ""CreateTestFiles.ino"" sketch first ") << ENDL;
	    while(1) delay(100);
	  }
	  RF.begin(115200);
	  xbc.begin(RF); // no return value
	  cmd.begin(xbc, &sd);

	  Serial << "Initialisation OK" << ENDL << ENDL;
}

void loop() {
	static uint32_t counter = 0;
	counter++;
  uint8_t stringSeqNbr;
	if (xbc.receive(myRecord, myString, stringType, stringSeqNbr, isRecord)) {
		counter=0;
		if (isRecord) {
			Serial << "Received a record. (ignored)";
		} else {
			Serial << "Received a string, with type=" << (int) stringType
					<< ENDL;
			Serial << "'" << myString << "'" << ENDL;
			if (stringType !=  CansatFrameType::CmdRequest ) {
				Serial << "Not a command, ignored. " << ENDL;
			}
			else cmd.processCmdRequest(myString);
		}
	}
	if (cmd.currentState() == RT_CanCommander::State_t::Acquisition) {
		// This where we would run the AcquisitionProcess in the can.
		// Checking the state regularly makes sure the CanCommander switches
		// back to Acquisition when its time-out expires
	}
	if (counter > 500000) {
		counter = 0;
		Serial << "Still alive (but not receiving anything)..." << ENDL;
	}
}
