/*
 * test_RT_CanCommander.ino
 *
 *
 * WARNING: This program allows for testing the RT_CanCommander in two different configurations:
 * 		-1- When RF API mode is NOT activated (symbol RF_ACTIVATE_API_MODE in CansatConfig.h)
 * 			The RT_CanCommander is not provided with an actual stream to the XBee module, but
 * 			with the usual Serial stream.
 * 			This allows for easy testing with a single board, running this sketch only but
 * 			does not test the code specific to the API-mode (mainly the processing of the
 * 			GetFile command.
 *
 * 			Wiring:  SD-Card reader on SPI bus, CS= CS_SD (configure below)
 *         			 LED on digital pin #LED_PinNbr
 *          		 Easiest solution is to use an Adalogger Feather M0 Express board,
 *          		 using only the on-board SD-Card reader and built-in LED.
 *
 *
 * 		-2- When RF API mode is activated, this program simulates the ground station,
 * 		    and does not host the RT_Commander. It must run on a board connected to
 * 		    the ground XBee module configured in CansatConfig.h
 * 		    See wiring information in program test_XBeeClient_Receive.
 *
 * 		    It will be connected, through the RF interface to the RT_CanCommander
 * 		    created by sketch test_OnBoardCanCommander. This sketch must run on a
 * 		    board connected to the corresponding can XBee module, as configured
 * 		    in CansatConfig.h (see wiring information in sketch
 * 		    test_XBeeClient_Send) with an SD-Card reader (same warning as above).
 *
 */
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

constexpr byte SD_CS=4;    // CS for SD-Card reader. If using an Adalogger Feather M0 Express board,
						   // the onboard SD reader is on pin 4 (which is not broken out).
constexpr byte LED_PinNbr=LED_BUILTIN;// pin connected to LED. LED_BUILTIN is the on-boar one.

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "ProjectRT_CanCommander.h"
#include "SdFat.h"
#include "SPI.h"
#include "CansatInterface.h"
#include "utilityFunctions.h"
#include "CansatXBeeClient.h"
#ifdef RF_ACTIVATE_API_MODE
#  include "elapsedMillis.h"
#endif

constexpr bool DebugSendCmdRequest=false;

#ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#  error "This program only works on Feather MO_Express"
#endif

/* Some commonly used commands: DO NOT HARDCODE numerical values: CansatInterface.h could be modified,
   and the test program should still work, unchanged. */
String initiateCmdMode;             /**< This command switches to the Command mode.*/
String terminateCmdMode;            /**< This command switches to the Acquisition Mode.*/

int numErrors = 0;
#ifdef RF_ACTIVATE_API_MODE
CansatXBeeClient xbc(CanXBeeAddressSH, CanXBeeAddressSL); // Defined in CansatConfig.h
HardwareSerial &RF = Serial1;
#else
SdFat sd;
ProjectRT_CanCommander cmd(10000);
#endif

// ===================== TEST FUNCTIONS =========================


void waitForResponse(unsigned long minDurationInMsec=500L) {
	// If using the transparent mode, no waiting is required: messages
	// are received synchronously. Wait anyway (useful for testing some timers).
	elapsedMillis ts=0;
#ifdef RF_ACTIVATE_API_MODE
	char str[xbc.MaxStringSize];
	CansatFrameType strType;
	uint8_t seqNbr, lastSeqNbr;
	elapsedMillis delaySinceLastReceived=0;
	bool receivingStringParts=false;
	uint32_t counter=0;
	while ((ts < minDurationInMsec) || (delaySinceLastReceived < 500)) {
		while (xbc.receive(str, strType, seqNbr))  {
			if (receivingStringParts) {
				if (strType == CansatFrameType::StringPart) {
					if (seqNbr != lastSeqNbr+1) {
						Serial << ENDL << "*** missed string part (expected" << lastSeqNbr+1
							   << ", got " << seqNbr << ")" << ENDL;
					}
					else lastSeqNbr++;
					Serial << str;
					counter+=strlen(str);
				} else {
					receivingStringParts=false;
					Serial << ENDL;
					Serial << "Received string parts for a total of "<< counter << " characters" << ENDL;
					Serial << "Received a string: type=" << (int) strType << ", content='" << str << "'" << ENDL;
				}
			} else {
				if (strType == CansatFrameType::StringPart) {
					receivingStringParts=true;
					lastSeqNbr=seqNbr;
					Serial << "Starting a string in several parts, with seq=" << seqNbr << ":" << ENDL;
					Serial << str;
					counter=strlen(str);
				} else {
					Serial << "Received a string: type=" << (int) strType << ", content='" << str << "'" << ENDL;
				}
			}
			delaySinceLastReceived=0;
		} // while received

	} // while duration
	// if still receiving string parts, there was no ENDL yet
	if (receivingStringParts) {
		Serial << ENDL;
		Serial << "Received string parts for a total of "<< counter << " characters" << ENDL;
	}
#else
	while (ts < minDurationInMsec) {
		delay(10);
	}
#endif

}

void sendCmdRequest(const char* req, unsigned long responseDelayInMsec=500)
{
	if (DebugSendCmdRequest) Serial << "sendCmdRequest for '" << req << "'";
#ifdef RF_ACTIVATE_API_MODE
	xbc.openStringMessage(CansatFrameType::CmdRequest);
	xbc << req;
	xbc.closeStringMessage(300);
	if (DebugSendCmdRequest) Serial << " (using RF)" << ENDL;
#else
	if (DebugSendCmdRequest) Serial << " (locally)" << ENDL;
	cmd.processCmdRequest(req);
#endif
	waitForResponse(responseDelayInMsec);
}

void testCommanderStateMachine() {
  String theCmd;
  Serial << "--- Testing RT-Commander state machine..." << ENDL;
  sendCmdRequest(terminateCmdMode.c_str());
#ifndef RF_ACTIVATE_API_MODE
  if (cmd.currentState() != RT_CanCommander::State_t::Acquisition) {
    Serial << F("Error: state should be Acquisition") << ENDL;
    numErrors++;
  }
#endif
  requestVisualCheck("Terminate Command Mode Sent");

  sendCmdRequest(initiateCmdMode.c_str());
#ifndef RF_ACTIVATE_API_MODE
  if (cmd.currentState() != RT_CanCommander::State_t::Command) {
    Serial << F("Error: state should be Command") << ENDL;
    numErrors++;
  }
#endif
  requestVisualCheck("Command Mode Requested");


  sendCmdRequest(terminateCmdMode.c_str());
#ifndef RF_ACTIVATE_API_MODE
  if (cmd.currentState() != RT_CanCommander::State_t::Acquisition) {
    Serial << F("Error: state should be Acquisition") << ENDL;
    numErrors++;
  }
#endif
  requestVisualCheck("Terminate Command mode Requested");

  Serial << F("Waiting for 10 secs in command mode to check time-out...") << ENDL;
  sendCmdRequest(initiateCmdMode.c_str());

#ifndef RF_ACTIVATE_API_MODE
  if (cmd.currentState() != RT_CanCommander::State_t::Command) {
    Serial << F("Error: state should be Command") << ENDL;
    numErrors++;
  }
#endif


#ifdef RF_ACTIVATE_API_MODE
  waitForResponse(12000);
  requestVisualCheck("Acquisition Mode should be restored.");
#else
  delay(12000);
  if (cmd.currentState() != RT_CanCommander::State_t::Acquisition) {
    Serial << F("Error: state should be Acquisition") << ENDL;
    numErrors++;
  } else Serial << F("OK") << ENDL;
#endif

  sendCmdRequest(initiateCmdMode.c_str());
  sendCmdRequest("meaninglessString");
  requestVisualCheck("Sent a meaningless string (should be ignored)");
  sendCmdRequest(terminateCmdMode.c_str());

  buildRequest(theCmd, CansatCmdRequestType::DigitalWrite);
  theCmd+="345"; // make an inexistent request type.
  sendCmdRequest(initiateCmdMode.c_str());
  sendCmdRequest(theCmd.c_str());
  requestVisualCheckWithCmd(theCmd, "Sent a meaningless request type");
  sendCmdRequest(terminateCmdMode.c_str());

  // Send a request, but without even a comma
  theCmd="String with no known type, no comma";
  sendCmdRequest(theCmd.c_str());
  requestVisualCheckWithCmd(theCmd, "Sent a request without type (should be ignored");
}

void testGenericPinCommands() { 
  Serial << "--- Testing pin-related commands" << ENDL;
  sendCmdRequest(initiateCmdMode.c_str());
  
  String theCmd;
  buildRequest(theCmd, CansatCmdRequestType::DigitalWrite, String(LED_PinNbr).c_str(), "1");
  sendCmdRequest(theCmd.c_str());
  requestVisualCheckWithCmd(theCmd, "Turning on LED");
  buildRequest(theCmd, CansatCmdRequestType::DigitalWrite, String(LED_PinNbr).c_str(), "0");
  sendCmdRequest(theCmd.c_str());
  requestVisualCheckWithCmd(theCmd, "Turning off LED");

  // How about the missing parameters?
  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(theCmd, CansatCmdRequestType::DigitalWrite);
  sendCmdRequest(theCmd.c_str());
  requestVisualCheckWithCmd(theCmd, "Sent an incomplete command: no parameter.");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(theCmd, CansatCmdRequestType::DigitalWrite, String(LED_PinNbr).c_str());
  sendCmdRequest(theCmd.c_str());
  requestVisualCheckWithCmd(theCmd, "Sent an incomplete command: no status.");

  sendCmdRequest(terminateCmdMode.c_str());
}

void testFileCommands() {
  Serial << "--- Testing file commands" << ENDL;
  sendCmdRequest(initiateCmdMode.c_str());
  String req;
  buildRequest(req, CansatCmdRequestType::ListFiles, "/");
  sendCmdRequest(req.c_str(),1500);
  requestVisualCheckWithCmd(req, "Root Directory Listed");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::ListFiles, "/Folder1");
  sendCmdRequest(req.c_str(),1000);
  requestVisualCheckWithCmd(req, "/Folder1 listed");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::ListFiles, "/Folder2");
  sendCmdRequest(req.c_str(),1000);
  requestVisualCheckWithCmd(req, "/Folder2 listed");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::ListFiles, "/InexistentFolder");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Inexistent folder Listed");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "/Folder1/file1.txt");
  sendCmdRequest(req.c_str(), 3000);
  requestVisualCheckWithCmd(req, "Requested content of /Folder1/file1.txt");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "/Folder1/file2.txt");
  sendCmdRequest(req.c_str(),3000);
  requestVisualCheckWithCmd(req, "Requested content of /Folder1/file2.txt");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "/Folder2/file3.txt");
  sendCmdRequest(req.c_str(),3000);
  requestVisualCheckWithCmd(req, "Requested content of /Folder2/file3.txt");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "/Folder2/file4.txt");
  sendCmdRequest(req.c_str(),3000);
  requestVisualCheckWithCmd(req, "Requested content of /Folder2/file4.txt");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "/file5.txt");
  sendCmdRequest(req.c_str(),3000);
  requestVisualCheckWithCmd(req, "Requested content of /file5.txt");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "/Folder2/inexistentFile.txt");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Requested content of inexistent file");
}

void testLargeFileCommands() {
  Serial << "Requesting content of large files..." << ENDL;
  String req;

  // Test get file piece by piece, with lines small enough to fit in buffer (lines never split for transmission)
  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "numbers.txt","0,55");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Requested first 55 bytes from numbers.txt file (first slice, beware end of line is \\r\\n, ie 2 characters))");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "numbers.txt","55,15");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Requested next 15 bytes from numbers.txt file (from byte 55). Did we get all of them? none skipped?");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "numbers.txt","70,15");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Requested next 15 bytes from numbers.txt file (from byte 70). Did we get all of them? none skipped?");

  // Test with a file with lines longer than the maximum buffersize (max 150 chars).
  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "longLine.txt","0,250");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Requested first 250 bytes from numbers.txt file (first slice, beware end of line is \\r\\n, ie 2 characters))");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "longLine.txt","250,250");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Requested next 250 bytes from longLine.txt file (from byte 250). Did we get all of them? none skipped?");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "longLine.txt","500,250");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Requested next 250 bytes from longLine.txt file (from byte 500). Did we get all of them? none skipped?");

  // Test with large files
  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "/FOLDER1/rec1000.txt","0,500");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Requested first 500 bytes from 1k record file (first slice");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "/FOLDER1/rec1000.txt","500,500");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Requested next 500 bytes from 1k record file (2nd slice");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "/FOLDER1/rec1000.txt","1000,10000");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Requested 10000 bytes from 1k record file (3nd slice)");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "/FOLDER1/rec1000.txt","500000,10000");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Requested bytes from position 500000 (after eof)");
  
  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "/FOLDER1/rec1000.txt","100000,10000");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Requested 10000 bytes from position 100000 (should reach eof at 101893)");
  
  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "/FOLDER2/rec10k.txt");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Requested 1st slice of 10k records file");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "/FOLDER2/rec10k.txt,60000,1000");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Requested 1000 char out of 10k records file at position 60000");

}

void testIncompleteFileCommands() {
  Serial << "--- Testing  INCOMPLETE file commands" << ENDL;
  String req;
  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::ListFiles);
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Missing path");

  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile);
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "Missing path");

  // List a directory which is a file
  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::ListFiles, "/file5.txt");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "listing a folder which is a file");

  // Get a file which is a directory
  sendCmdRequest(initiateCmdMode.c_str());
  buildRequest(req, CansatCmdRequestType::GetFile, "/FOLDER1");
  sendCmdRequest(req.c_str());
  requestVisualCheckWithCmd(req, "getting a file which is a folder");
}

void testProjectSpecificCommands() {
	String req;
	// Test an existing command.
	sendCmdRequest(initiateCmdMode.c_str());
	buildRequest(req, (int) ProjectCmdRequest::aProjectCommand,	"args to project command");
	sendCmdRequest(req.c_str());
	requestVisualCheckWithCmd(req, "Sending a valid project command (#100) (expecting response 101)");
}

void testShutdownCommands() {
	String req;
	// Test PrepareShutdown.
	sendCmdRequest(initiateCmdMode.c_str());
	buildRequest(req, (int) CansatCmdRequestType::PrepareShutdown);
	sendCmdRequest(req.c_str());
	requestVisualCheckWithCmd(req, "Sending a PrepareShutdown, expecting response 'ReadyForShutdown'");
	// Test PrepareShutdown.
	sendCmdRequest(initiateCmdMode.c_str());
	buildRequest(req, (int) CansatCmdRequestType::CancelShutdown);
	sendCmdRequest(req.c_str());
	requestVisualCheckWithCmd(req, "Sending a CancelShutdown, expecting response 'ShutdownCancelled'");
}

// ================================== SETUP ================================

void setup () {
  DINIT(115200);
#ifdef RF_ACTIVATE_API_MODE
  RF.begin(115200);
  xbc.begin(RF); // no return value
  Serial << "RT-CanCommander unit test in API mode: this is the ground part" << ENDL;
  Serial << "Make sure the test_onBoardCommander sketch runs on the can board" << ENDL;
#else
  cmd.begin(Serial, &sd);
  pinMode(LED_PinNbr, OUTPUT);
  digitalWrite(LED_PinNbr, LOW);
  Serial << "RT-CanCommander unit test (simulating TRANSPARENT mode" << ENDL;
  Serial << "  SD-Card CS = " << SD_CS << ENDL;
  Serial << "  LED is on pin #" << LED_PinNbr << ENDL;
  SPI.begin();
  if (!sd.begin(SD_CS)) {
    sd.initErrorHalt();
  }
  Serial << "  SD-Card OK" << ENDL;
  if (!sd.exists("Folder1")) {                              /**< Tests the existance of Folder1*/
    Serial << F("Test Folder1 cannot be found. Please run the ""CreateTestFiles.ino"" sketch first") << ENDL;
    while(1) delay(100);
  }
  if (!sd.exists("Folder2")) {                              /**< Tests the existance of Folder2*/
    Serial << F("Test Folder2 cannot be found. Please run the ""CreateTestFiles.ino"" sketch first ") << ENDL;
    while(1) delay(100);
  }
#endif
  // Prepare frequent commands
  buildRequest(initiateCmdMode, CansatCmdRequestType::InitiateCmdMode);
  buildRequest(terminateCmdMode, CansatCmdRequestType::TerminateCmdMode);
  
  Serial << "Initialization OK" << ENDL;
  Serial << "-----------------------------------------------"  << ENDL;

  testCommanderStateMachine();
  testGenericPinCommands();
  testFileCommands();

  testLargeFileCommands();
  testIncompleteFileCommands();
  testShutdownCommands();
  testProjectSpecificCommands();
  
  Serial << F("Number Of Errors: ") << numErrors << ENDL;
}

// ================================== LOOP ================================
void loop () {}
