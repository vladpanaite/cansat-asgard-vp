/*
 * test_CansatRecord.ino
 *
 * Test program for class CansatRecord.
 *
 */
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatRecord.h"
#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "StringStream.h"
#include "CansatRecordExample.h"

#define TEST_HUMAN_READABLE_PRINT  // undefine to avoid testing non operational code. 

unsigned int numErrors = 0;
String strBuffer;
StringStream strStream(strBuffer);

void requestVisualCheck() {
	char answer = ' ';
	Serial << ENDL;
	// remove any previous character in input queue
	while (Serial.available() > 0) {
		Serial.read();
	}

	while ((answer != 'y') && (answer != 'n')) {
		Serial << "Is this ok (y/n) ? ";
		Serial.flush();
		// remove any previous character in input queue
		while (Serial.available() == 0) {
			delay(300);
		}
		answer = Serial.read();
	}
	if (answer == 'n') {
		numErrors++;
	}
	Serial << ENDL;
}

// This class is a friend of CansatRecord and can access internals.
class CansatRecord_Test {
public:
	CansatRecord_Test(CansatRecord& theRecord) :
			record(theRecord) {
	}

	bool checkString(String &actual, const char * expected) {
		if (actual != expected) {
			Serial << F(" **** Got '") <<actual << F("' instead of '") <<expected << "'" << ENDL;
			numErrors++;
			return false; 
		}
		else  {
			Serial << F("OK") << ENDL;
			return true;
		}
	}

	void testIndividualPrints() {
		strBuffer="";
		Serial << F("Printing a CSV Float with NO final separator") << ENDL;
		record.printCSV(strStream, (float) 12.3);
		checkString(strBuffer, "12.30");
		Serial << F("Printing a CSV Float with a final separator") << ENDL;
		strBuffer="";
		record.printCSV(strStream, (float) 12.3, true);
		checkString(strBuffer, "12.30,");

		Serial	<< "Printing a CSV Float with NO final separator, with 3 trailing decimals"
				<< ENDL;
		strBuffer="";
		record.printCSV(strStream, (float) 12.3, false, 3);
		checkString(strBuffer, "12.300");

		Serial  << "Printing a CSV Float with NO final separator, with 1 trailing decimals"
				<< ENDL;
		strBuffer="";
		record.printCSV(strStream, (float) 12.321, false, 1);
		checkString(strBuffer, "12.3");

		bool mybool = true;
		Serial << F("Printing a CSV bool with NO final separator") << ENDL;
		strBuffer="";
		record.printCSV(strStream, mybool, false);
		checkString(strBuffer, "1");
		Serial << F("Printing a CSV bool with a final separator") << ENDL;
		strBuffer="";
		record.printCSV(strStream, mybool, true);
		checkString(strBuffer, "1,");
	}
	CansatRecord& record;
};


void assignValues(CansatRecord& rec) {
	rec.timestamp = 1;

	rec.newGPS_Measures = true;
	rec.GPS_LatitudeDegrees = 25.25222;
	rec.GPS_LongitudeDegrees = 26.25876;
	rec.GPS_Altitude = 27.269;
#ifdef INCLUDE_GPS_VELOCITY
	rec.GPS_VelocityKnots = 28.2805;
	rec.GPS_VelocityAngleDegrees = 29.29321;
#endif

	rec.temperatureBMP = 36.3577;
	rec.pressure = 37.3689;
	rec.altitude = 38.38431;
#ifdef INCLUDE_REFERENCE_ALTITUDE
  rec.refAltitude = 27.28431;
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
    rec.descentVelocity=39.39112;
#endif
	rec.temperatureThermistor1 = 39.38655;
#ifdef INCLUDE_THERMISTOR2
	rec.temperatureThermistor2 = 40.3973;
#endif
#ifdef INCLUDE_THERMISTOR3
	rec.temperatureThermistor3 = 41.4121;
#endif
}

static bool checkFloat(float actual, float expected, float accuracy=0.0001) {
	if (fabs(actual - expected) < accuracy) {
		return true;
	}else {
		numErrors++;
		Serial << F(" *** ERROR: float does not match (accuracy=") << accuracy << F("). Got ") << actual << F(" instead of ") << expected << ENDL;
		return false;
	}
}
void checkValues(CansatRecord& rec) {
	DASSERT(rec.timestamp == 1);

	DASSERT(rec.newGPS_Measures == true);
	DASSERT(checkFloat(rec.GPS_LatitudeDegrees, 25.25222, 0.00001));
	DASSERT(checkFloat(rec.GPS_LongitudeDegrees , 26.25876,0.00001));
	DASSERT(checkFloat(rec.GPS_Altitude , 27.3,0.1));
#ifdef INCLUDE_GPS_VELOCITY
	DASSERT(checkFloat(rec.GPS_VelocityKnots , 28.28,0.01));
	DASSERT(checkFloat(rec.GPS_VelocityAngleDegrees , 29.29,0.01));
#endif

	DASSERT(checkFloat(rec.temperatureBMP, 36.4, 0.1));
	DASSERT(checkFloat(rec.pressure , 37.4, 0.1));
	DASSERT(checkFloat(rec.altitude , 38.4, 0.1));
#ifdef INCLUDE_REFERENCE_ALTITUDE
  DASSERT(checkFloat(rec.refAltitude , 27.3, 0.1));
#endif
#ifdef INCLUDE_DESCENT_VELOCITY
	DASSERT(checkFloat(rec.descentVelocity,39.4, 0.1));
#endif
	DASSERT(checkFloat(rec.temperatureThermistor1 , 39.4, 0.1));
#ifdef INCLUDE_THERMISTOR2
	DASSERT(checkFloat(rec.temperatureThermistor2 , 40.4, 0.1));
#endif
#ifdef INCLUDE_THERMISTOR3
	DASSERT(checkFloat(rec.temperatureThermistor3, 41.41, 0.1));
#endif
}


void performTestOnPlainCansatRecord() {
	CansatRecord record;
	assignValues(record);
	checkValues(record);
	Serial << ENDL << F("******************************************") << ENDL;
	Serial <<         F("*** A. Testing the CansatRecord itself ***") << ENDL;
	Serial <<         F("******************************************") << ENDL << ENDL;

	CansatRecord_Test friendClass(record);
	Serial << F("A.1. Testing individual print utilities") << ENDL;
	friendClass.testIndividualPrints();

	Serial
			<< F("A.2 Values Assigned, printing complete record in CSV format (with header):")
			<< ENDL;
	record.printCSV(Serial, CansatRecord::DataSelector::All,
			CansatRecord::HeaderOrContent::Header);
	Serial << ENDL;
  strBuffer="";
  record.printCSV(strStream, CansatRecord::DataSelector::All,
      CansatRecord::HeaderOrContent::Header);
  Serial << F("CSV Header is ") << strBuffer.length() << F(" characters") << ENDL;
  if (strBuffer.length() != record.getCSV_HeaderSize()) {
    Serial << F("*** Error: expected ") << record.getCSV_HeaderSize() << ENDL;
    numErrors++;
  }
	record.printCSV(Serial);
	requestVisualCheck();

	Serial << ENDL << F("A.3 Testing CansatRecord Clear(): ") << ENDL;
	record.clear();
	record.printCSV(Serial);
	Serial << ENDL;
	DASSERT(record.newGPS_Measures == false);
	DASSERT(record.timestamp == 0);
	DASSERT(record.GPS_LatitudeDegrees == 0);
	DASSERT(record.GPS_LongitudeDegrees == 0);
	DASSERT(record.GPS_Altitude == 0);
#ifdef INCLUDE_GPS_VELOCITY
	DASSERT(record.GPS_VelocityKnots == 0.00);
	DASSERT(record.GPS_VelocityAngleDegrees == 0.00);
#endif
	DASSERT(record.temperatureBMP == 0);
	DASSERT(record.pressure == 0);
	DASSERT(record.altitude == 0);
#ifdef INCLUDE_DESCENT_VELOCITY
    DASSERT(record.descentVelocity == 0);
#endif
	DASSERT(record.temperatureThermistor1 == 0);
#ifdef INCLUDE_THERMISTOR2
	DASSERT(record.temperatureThermistor2 == 0);
#endif
#ifdef INCLUDE_THERMISTOR3
	DASSERT(record.temperatureThermistor3 == 0);
#endif

	Serial << F("   Values checked OK. ") << ENDL;

#ifdef TEST_HUMAN_READABLE_PRINT
	assignValues(record);
	Serial << F("A.4 Printing complete record in Human Readable Format:") << ENDL;
	record.print(Serial);
	Serial << ENDL;
	requestVisualCheck();
#endif

	Serial << ENDL << F("A.5 checking maximum sizes") << ENDL;
	strBuffer="";
	record.printCSV(strStream, CansatRecord::DataSelector::All,
			CansatRecord::HeaderOrContent::Header);
	Serial << F("Length of header is ") << strBuffer.length() << F(", header size supposed to be ")
			<< record.getCSV_HeaderSize() <<ENDL;
	if (strBuffer.length() != record.getCSV_HeaderSize()) {
		numErrors++;
		Serial << F(" **** Error on header length ****") << ENDL;
	}
	strBuffer="";  // clear string stream.
	record.printCSV(strStream);
	Serial << F("Length of CSV record is ") << strBuffer.length() << F(", record max size is ")
			<< record.getMaxCSV_Size() <<ENDL;
	if (strBuffer.length() > record.getCSV_HeaderSize()) {
		numErrors++;
		Serial << F(" **** Error on header length ****") << ENDL;
	}

	Serial << ENDL << F("A.6 binary serialization") << ENDL;
	assignValues(record);
	uint8_t bufferSize= record.getBinarySize();
	Serial << "  Binary size=" << bufferSize << ENDL;
	uint8_t buffer[bufferSize];
	if (!record.writeBinary(buffer, bufferSize)) {
		Serial << F(" *** Error while writing as binary") << ENDL;
		numErrors++;
	} else {
		Serial << F("Record written") << ENDL;
	}
	CansatRecord otherRecord;
	if (!otherRecord.readBinary(buffer, bufferSize)) {
		Serial << F(" *** Error while reading as binary") << ENDL;
		numErrors++;
	} else {
		Serial << F("Record read, checking values.") << ENDL;
		checkValues(otherRecord);
	}

}

void performTestOnSubclass() {
	Serial << ENDL << F("*** B. Testing a subclass of CansatRecord (xxxxRecord), adding 2 floats for dataA and data B ***") << ENDL;

	CansatRecordExample record;
	record.initValues();
	record.checkValues();

	Serial << F("B.1 Values Assigned, printing complete record in CSV format (with header):")
			<< ENDL;
	record.printCSV(Serial, CansatRecord::DataSelector::All,
			CansatRecord::HeaderOrContent::Header);  
  strBuffer="";
  record.printCSV(strStream, CansatRecord::DataSelector::All,
    CansatRecord::HeaderOrContent::Header);
  Serial << ENDL;
  Serial << F("CSV Header is ") << strBuffer.length() << F(" characters") << ENDL;
  if (strBuffer.length() != record.getCSV_HeaderSize()) {
    Serial << F("*** Error: expected ") << record.getCSV_HeaderSize() << ENDL;
    numErrors++;
  } 
	Serial << ENDL;
	record.printCSV(Serial);
	requestVisualCheck();
	Serial << F("Printing complete record except secondary mission in CSV format:") << ENDL;
	record.printCSV(Serial, CansatRecord::DataSelector::AllExceptSecondary,
			CansatRecord::HeaderOrContent::Header);
	Serial << ENDL;
	record.printCSV(Serial, CansatRecord::DataSelector::AllExceptSecondary);
	requestVisualCheck();
	Serial << F("Printing primary mission only in CSV format:") << ENDL;
	record.printCSV(Serial, CansatRecord::DataSelector::Primary);
	record.printCSV(Serial, CansatRecord::DataSelector::Primary);
	requestVisualCheck();
	Serial << F("Printing secondary mission only in CSV format:") << ENDL;
	record.printCSV(Serial, CansatRecord::DataSelector::Secondary,
			CansatRecord::HeaderOrContent::Header);
	Serial << ENDL;
	record.printCSV(Serial, CansatRecord::DataSelector::Secondary);
	requestVisualCheck();
	Serial << F("Printing primary+secondary mission only in CSV format:") << ENDL;
	record.printCSV(Serial, CansatRecord::DataSelector::PrimarySecondary,
			CansatRecord::HeaderOrContent::Header);
	Serial << ENDL;
	record.printCSV(Serial, CansatRecord::DataSelector::PrimarySecondary);
	requestVisualCheck();
	Serial << F("Printing GPS data only in CSV format:") << ENDL;
	record.printCSV(Serial, CansatRecord::DataSelector::GPS,
			CansatRecord::HeaderOrContent::Header);
	Serial << ENDL;
	record.printCSV(Serial, CansatRecord::DataSelector::GPS);
	requestVisualCheck();

	Serial << ENDL << F("B.2 Testing TestCansatRecord::Clear(): ") << ENDL;
	record.clear();
	record.printCSV(Serial);
	Serial << ENDL;
	DASSERT(record.newGPS_Measures == false);
	DASSERT(record.timestamp == 0);
	DASSERT(record.GPS_LatitudeDegrees == 0);
	DASSERT(record.GPS_LongitudeDegrees == 0);
	DASSERT(record.GPS_Altitude == 0);
#ifdef INCLUDE_GPS_VELOCITY
	DASSERT(record.GPS_VelocityKnots == 0.00);
	DASSERT(record.GPS_VelocityAngleDegrees == 0.00);
#endif
	DASSERT(record.temperatureBMP == 0);
	DASSERT(record.pressure == 0);
	DASSERT(record.altitude == 0);
	DASSERT(record.temperatureThermistor1 == 0);
#ifdef INCLUDE_THERMISTOR2
	DASSERT(record.temperatureThermistor2 == 0);
#endif
#ifdef INCLUDE_THERMISTOR3
	DASSERT(record.temperatureThermistor3 == 0);
#endif
    DASSERT(record.integerData==0);
    DASSERT(record.floatData==0);
	Serial << F("   Values checked OK. ") << ENDL;

#ifdef TEST_HUMAN_READABLE_PRINT
	record.initValues();
	Serial << F("B.3 Printing complete record in Human Readable Format:") << ENDL;
	record.print(Serial);
	Serial << ENDL;
	requestVisualCheck();
	Serial << F("Printing complete record except secondary mission in Human Readable Format:") << ENDL;
	record.print(Serial, CansatRecord::DataSelector::AllExceptSecondary);
	requestVisualCheck();
	Serial << F("Printing primary mission only in Human Readable Format:") << ENDL;
	record.print(Serial, CansatRecord::DataSelector::Primary);
	requestVisualCheck();
	Serial << F("Printing secondary mission only in Human Readable Format:") << ENDL;
	record.print(Serial, CansatRecord::DataSelector::Secondary);
	requestVisualCheck();
	Serial << F("Printing primary+secondary mission only in Human Readable Format:") << ENDL;
	record.print(Serial, CansatRecord::DataSelector::PrimarySecondary);
	requestVisualCheck();
	Serial << F("Printing GPS data only in Human Readable Format:") << ENDL;
	record.print(Serial, CansatRecord::DataSelector::GPS);
	requestVisualCheck();
#endif

	Serial << F("B.4 checking maximum sizes") << ENDL;
  strBuffer="";
	record.printCSV(strStream, CansatRecord::DataSelector::All,
			CansatRecord::HeaderOrContent::Header);
	Serial << F("Length of header is ") << strBuffer.length() 
	       << F(", header size supposed to be ")
			   << record.getCSV_HeaderSize() <<ENDL;
	if (strBuffer.length() != record.getCSV_HeaderSize()) {
		numErrors++;
		Serial << F(" **** Error on header length ****") << ENDL;
    Serial << F("Header: '") << strBuffer << F("'") << ENDL;
	}
	strBuffer="";  // clear string stream.
	record.printCSV(strStream);
	Serial << F("Length of CSV record is ") << strBuffer.length() 
	       << F(", record max size is ")
			   << record.getMaxCSV_Size() <<ENDL;
	if (strBuffer.length() > record.getCSV_HeaderSize()) {
		numErrors++;
		Serial << F(" **** Error on record length ****") << ENDL;
	}

	Serial << ENDL << F("B.5 binary serialization") << ENDL;
	record.initValues();
  record.print(Serial);
	uint8_t bufferSize= record.getBinarySize();
	Serial << F("  Binary size=") << bufferSize << ENDL;
	uint8_t buffer[bufferSize];
	if (!record.writeBinary(buffer, bufferSize)) {
		Serial << F(" *** Error while writing as binary") << ENDL;
		numErrors++;
	} else {
		Serial << F("Record written") << ENDL;
	}
	CansatRecordExample otherRecord;
	if (!otherRecord.readBinary(buffer, bufferSize)) {
		Serial << F(" *** Error while reading as binary") << ENDL;
		numErrors++;
	} else {
		Serial << F("Record read, checking values.") << ENDL;
		otherRecord.print(Serial);
		if (!otherRecord.checkValues()){
			numErrors++;
		}
	}

}
void setup() {
	Serial.begin(115200);
	while (!Serial)
		;
	Serial << F("Test of CansatRecord and a subclass ") << ENDL;
	Serial << F("Default number of decimal positions for floats:")
			<< CansatRecord::NumDecimalPositions_Default << ENDL;
  strBuffer.reserve(180); // On Arduino Uno, not reserving seems to limit length to 160 bytes. Why ? 
  
	//assignValues(record);
	performTestOnPlainCansatRecord();
	performTestOnSubclass();
	Serial << ENDL << F("End of job. Total number of errors:") << numErrors << ENDL;
}

void loop() {
}
