/*
   Test for class StorageManager

   EEPROM storage is not tested if IGNORE_EEPROMS is not defined in CansatConfig.h

   NB: EEPROMS not tested after review of 2020.
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "CansatConfig.h"

#include "SD_Logger.h"



#include "StorageManager.h"
#ifndef IGNORE_EEPROMS
#include "EEPROM_BankWithTools.h"
#endif

const byte chipSelectPinNumber = 4;

HardwareScanner hw;
StorageManager sm(2, 3 );
CansatRecord record;

#ifndef IGNORE_EEPROMS
const EEPROM_BankWriter::EEPROM_Key EEPROM_KeyValue = 0x1234;
#endif

void clearEEPROM()
{
#ifndef IGNORE_EEPROMS
  // First clear EEPROM
  EEPROM_Bank bank(5);
  bank.init(EEPROM_KeyValue, hw, sizeof(MyData));
  bank.erase();
#endif
}

void checkData() {
#ifndef IGNORE_EEPROMS
  // Check the content of the EEPROM
  Serial << ENDL << F("Checking data in EEPROM...") << ENDL;
  EEPROM_Bank bank(5);
  bank.init(EEPROM_KeyValue, hw, sizeof(MyData));
  Serial << F("Found ") << bank.recordsLeftToRead() << F(" records in EEPROM") << ENDL;
  DASSERT(bank.recordsLeftToRead() == 2);

  md.a = 0;
  md.b = 0;
  bank.readOneRecord((byte* )&md, sizeof(md));
  DASSERT(md.a == 5);
  DASSERT(md.b == 3.141592);
  bank.readOneRecord((byte*) &md, sizeof(md));
  DASSERT(md.a == 6);
  DASSERT(md.b == 3.141592);
#endif

  // Check on the SD-Card ?

}
//----------------------------------------------------------------//
void setup() {
  DINIT(9600);
  Serial << F("testing StorageManager...") << ENDL;
  Serial << F("Size of StorageManager:") << sizeof(StorageManager) << ENDL;
  //delay(5)

  Serial << F("FreeRam: ") << freeRam() << ENDL;
  hw.init(1, 127, 400);
  clearEEPROM();

  String msg = "dummy first line";
  bool smOK = sm.init(record, &hw, msg);
  Serial << F("StorageManager::init() OK: ") << smOK << ENDL;
  Serial << F("Size of storageManager: ") << sizeof(sm) << F(" bytes") << ENDL;

  //init of record
  record.timestamp = 5;
  record.GPS_LatitudeDegrees = 3.141592;
  //init of aString
  String aString = F("Bonjour");

  Serial << F("Testing storeOneRecord()...") << ENDL;
  Serial << F("FreeRam: ") << freeRam() << ENDL;
  sm.storeOneRecord(record);
  record.timestamp++;
  sm.storeOneRecord(record);
  sm.storeString("Bonjour");

  Serial << F("calling doIdle() during 10 secs...") << ENDL;
  for (int i = 0; i < 20 ; i++ ) {
    sm.doIdle();
    Serial << ".";
    delay(500);
  }

  if (smOK) {
    checkData();
  }

  Serial << F("end of tests:") << ENDL;
  Serial << F("Please manually check that SD-Card contains 1 new file, prefix ',")
		 << SD_FilesPrefix <<"'" << ENDL;
  Serial << F("containing a first line '") << msg
		 << F("', 2 records (CSV) with timestamps 5 and 6") << ENDL
		 << F(" and a last line 'Bonjour'") << ENDL;
}

void loop() {
  // No loop

}
