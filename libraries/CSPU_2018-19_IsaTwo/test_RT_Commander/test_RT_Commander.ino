/*
   test_RT_Commander.ino

   Wiring:  SD-Card reader on SPI bus, CS= CS_SD (configure below)
            LED on digital pin #LED_PinNbr
*/

constexpr byte SD_CS=13;    // CS for SD-Card reader
constexpr byte LED_PinNbr=6;// pin connected to LED.

#include "RT_Commander.h"
#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "SdFat.h"
#include "SPI.h"
#include "IsaTwoInterface.h"
#include "utilityFunctions.h"

/* Some commonly used commands: DO NOT HARDCODE numerical values: IsaTwoInterface.h could be modified,
   and the test program should still work, unchanged. */
String initiateCmdMode;                     /**< This command switches to the Command mode.*/
String terminateCmdMode;                      /**< This command switches to the Acquisition Mode.*/

int numErrors = 0;
SdFat sd;
RT_Commander cmd((int) IsaTwoRecordType::CmdRequest, 10000);

// ===================== TEST FUNCTIONS =========================

void testCommanderStateMachine() {
  String theCmd;
  Serial << "--- Testing RT-Commander state machine..." << ENDL;
  cmd.processMsg(terminateCmdMode.c_str());
  if (cmd.currentState() != RT_Commander::State_t::Acquisition) {
    Serial << F("Error: state should be Acquisition") << ENDL;
    numErrors++;
  }
  requestVisualCheck("Terminate Command Mode Sent");

  cmd.processMsg(initiateCmdMode.c_str());
  if (cmd.currentState() != RT_Commander::State_t::Command) {
    Serial << F("Error: state should be Command") << ENDL;
    numErrors++;
  }
  requestVisualCheck("Command Mode Requested");

  cmd.processMsg(terminateCmdMode.c_str());
  if (cmd.currentState() != RT_Commander::State_t::Acquisition) {
    Serial << F("Error: state should be Acquisition") << ENDL;
    numErrors++;
  }
  requestVisualCheck("Terminate Command mode Requested");

  cmd.processMsg(initiateCmdMode.c_str());
  if (cmd.currentState() != RT_Commander::State_t::Command) {
    Serial << F("Error: state should be Command") << ENDL;
    numErrors++;
  }

  Serial << F("Waiting for 10 secs to check time-out...");
  delay(10000);
  if (cmd.currentState() != RT_Commander::State_t::Acquisition) {
    Serial << F("Error: state should be Acquisition") << ENDL;
    numErrors++;
  } else Serial << F("OK") << ENDL;

  cmd.processMsg(initiateCmdMode.c_str());
  cmd.processMsg("meaninglessString");
  requestVisualCheck("Sent a meaningless string (should be ignored)");
  cmd.processMsg(terminateCmdMode.c_str());
 
  buildRequest(theCmd, IsaTwoCmdRequestType::DigitalWrite);
  theCmd+="345"; // make an inexistent request type.
  cmd.processMsg(initiateCmdMode.c_str());
  cmd.processMsg(theCmd.c_str());
  requestVisualCheckWithCmd(theCmd, "Sent a meaningless request type");
  cmd.processMsg(terminateCmdMode.c_str());

  // Send a request, but without even a comma
  theCmd=(int) IsaTwoRecordType::CmdRequest;
  cmd.processMsg(theCmd.c_str());
  requestVisualCheckWithCmd(theCmd, "Sent a request without type (should be ignored");
}

void testGenericPinCommands() { 
  Serial << "--- Testing pin-related commands" << ENDL;
  cmd.processMsg(initiateCmdMode.c_str());
  
  String theCmd;
  buildRequest(theCmd, IsaTwoCmdRequestType::DigitalWrite, String(LED_PinNbr).c_str(), "1");
  cmd.processMsg(theCmd.c_str());
  requestVisualCheckWithCmd(theCmd, "Turning on LED");
  buildRequest(theCmd, IsaTwoCmdRequestType::DigitalWrite, String(LED_PinNbr).c_str(), "0");
  cmd.processMsg(theCmd.c_str());
  requestVisualCheckWithCmd(theCmd, "Turning off LED");

  // How about the missing parameters?
  cmd.processMsg(initiateCmdMode.c_str());
  buildRequest(theCmd, IsaTwoCmdRequestType::DigitalWrite);
  cmd.processMsg(theCmd.c_str());
  requestVisualCheckWithCmd(theCmd, "Sent an incomplete command: no parameter.");

  cmd.processMsg(initiateCmdMode.c_str());
  buildRequest(theCmd, IsaTwoCmdRequestType::DigitalWrite, String(LED_PinNbr).c_str());
  cmd.processMsg(theCmd.c_str());
  requestVisualCheckWithCmd(theCmd, "Sent an incomplete command: no status.");

  cmd.processMsg(terminateCmdMode.c_str());
}

void testFileCommands() {
  Serial << "--- Testing file commands" << ENDL;
  cmd.processMsg(initiateCmdMode.c_str());
  String req;
  buildRequest(req, IsaTwoCmdRequestType::ListFiles, "/");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Root Directory Listed");

  buildRequest(req, IsaTwoCmdRequestType::ListFiles, "/Folder1");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "/Folder1 listed");

  buildRequest(req, IsaTwoCmdRequestType::ListFiles, "/Folder2");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "/Folder2 listed");

  buildRequest(req, IsaTwoCmdRequestType::ListFiles, "/InexistentFolder");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Inexistent folder Listed");

  buildRequest(req, IsaTwoCmdRequestType::GetFile, "/Folder1/file1.txt");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Requested content of /Folder1/file1.txt");

  buildRequest(req, IsaTwoCmdRequestType::GetFile, "/Folder1/file2.txt");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Requested content of /Folder1/file2.txt");

  buildRequest(req, IsaTwoCmdRequestType::GetFile, "/Folder2/file3.txt");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Requested content of /Folder2/file3.txt");

  buildRequest(req, IsaTwoCmdRequestType::GetFile, "/Folder2/file4.txt");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Requested content of /Folder2/file4.txt");

  buildRequest(req, IsaTwoCmdRequestType::GetFile, "/file5.txt");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Requested content of /file5.txt");

  buildRequest(req, IsaTwoCmdRequestType::GetFile, "/Folder2/inexistentFile.txt");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Requested content of inexistent file");
}

void testLargeFileCommands() {
  Serial << "Requesting content of large files..." << ENDL;
  String req;
  cmd.processMsg(initiateCmdMode.c_str());
  buildRequest(req, IsaTwoCmdRequestType::GetFile, "/FOLDER1/rec1000.txt","0,500");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Requested first 500 bytes from 1k record file (first slice");

  cmd.processMsg(initiateCmdMode.c_str());
  buildRequest(req, IsaTwoCmdRequestType::GetFile, "/FOLDER1/rec1000.txt","500,500");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Requested next 500 bytes from 1k record file (2nd slice");

  cmd.processMsg(initiateCmdMode.c_str());
  buildRequest(req, IsaTwoCmdRequestType::GetFile, "/FOLDER1/rec1000.txt","1000,10000");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Requested 10000 bytes from 1k record file (3nd slice)");

  cmd.processMsg(initiateCmdMode.c_str());
  buildRequest(req, IsaTwoCmdRequestType::GetFile, "/FOLDER1/rec1000.txt","500000,10000");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Requested bytes from position 500000 (after eof)");
  
  cmd.processMsg(initiateCmdMode.c_str());
  buildRequest(req, IsaTwoCmdRequestType::GetFile, "/FOLDER1/rec1000.txt","113000,10000");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Requested 10000 bytes from position 113000 (should reach eof at 114000)");
  
  cmd.processMsg(initiateCmdMode.c_str());
  buildRequest(req, IsaTwoCmdRequestType::GetFile, "/FOLDER2/rec10k.txt");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Requested 1st slice of 10k records file");

    cmd.processMsg(initiateCmdMode.c_str());
  buildRequest(req, IsaTwoCmdRequestType::GetFile, "/FOLDER2/rec10k.txt,1120000,1000");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Requested 1000char out of 10k records file at position 1120000");

}

void testIncompleteFileCommands() {
  Serial << "--- Testing  INCOMPLETED file commands" << ENDL;
  String req;
  cmd.processMsg(initiateCmdMode.c_str());
  buildRequest(req, IsaTwoCmdRequestType::ListFiles);
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Missing path");

  cmd.processMsg(initiateCmdMode.c_str());
  buildRequest(req, IsaTwoCmdRequestType::GetFile);
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "Missing path");

  // List a directory which is a file
  cmd.processMsg(initiateCmdMode.c_str());
  buildRequest(req, IsaTwoCmdRequestType::ListFiles, "/file5.txt");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "listing a folder which is a file");

  // Get a file which is a directory
  cmd.processMsg(initiateCmdMode.c_str());
  buildRequest(req, IsaTwoCmdRequestType::GetFile, "/FOLDER1");
  cmd.processMsg(req.c_str());
  requestVisualCheckWithCmd(req, "getting a file which is a folder");
}

// ================================== SETUP ================================
void setup () {
  DINIT(115200);
  cmd.begin(Serial, &sd);
  pinMode(LED_PinNbr, OUTPUT);
  digitalWrite(LED_PinNbr, LOW);
  Serial << "RT-Commander unit test " << ENDL;
  Serial << "  SD-Card CS = " << SD_CS << ENDL;
  Serial << "  LED is on pin #" << LED_PinNbr << ENDL;
  SPI.begin();
  if (!sd.begin(SD_CS)) {
    sd.initErrorHalt();
  }
  Serial << "  SD-Card OK" << ENDL;
  if (!sd.exists("Folder1")) {                              /**< Tests the existance of Folder1*/
    Serial << F("Test Folder1 cannot be found. Please run the ""CreateTestFiles.ino"" sketch first") << ENDL;
    while(1) delay(100);
  }
  if (!sd.exists("Folder2")) {                              /**< Tests the existance of Folder2*/
    Serial << F("Test Folder2 cannot be found. Please run the ""CreateTestFiles.ino"" sketch first ") << ENDL;
    while(1) delay(100);
  }

  // Prepare frequent commands
  buildRequest(initiateCmdMode, IsaTwoCmdRequestType::InitiateCmdMode);
  buildRequest(terminateCmdMode, IsaTwoCmdRequestType::TerminateCmdMode);
  
  Serial << "Initialisation OK" << ENDL;
  Serial << "-----------------------------------------------"  << ENDL;

  testCommanderStateMachine();
  testGenericPinCommands();
  testFileCommands();
  testLargeFileCommands();
  testIncompleteFileCommands();
  
  Serial << F("Number Of Errors: ") << numErrors << ENDL;
}

// ================================== LOOP ================================
void loop () {}
