#include "GMiniRecord.h"
#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "StringStream.h"

#define DBG 1

class GMiniRecord_Test {
  public:
    void testClear(GMiniRecord &rec) {
      DPRINTLN(DBG, "Test: clearing record [BEGIN]");

      initValues(rec);
      clearValues(rec);

      checkClearedValues(rec);

      DPRINTLN(DBG, "Test successful");
      DPRINTLN(DBG, "Test: clearing record [END]");
    }
    void testCSVLen(GMiniRecord &rec) {
      DPRINTLN(DBG, "Test: CSV length [BEGIN]");
      initValues(rec);

      String s;
      StringStream ss(s);

      rec.printCSV_SecondaryMissionData(ss, true, true);
      DASSERT(rec.getSecondaryMissionMaxCSV_Size() >= s.length());

      s = "";

      rec.printCSV_SecondaryMissionHeader(ss, true, false);
      DASSERT(rec.getSecondaryMissionCSV_HeaderSize() == s.length());

      DPRINTLN(DBG, "Test successful");
      DPRINTLN(DBG, "Test: CSV length [END]");
    }
    void testReadWriteBinary(GMiniRecord &rec) {
      DPRINTLN(DBG, "Test: read/write binary [BEGIN]");

      initValues(rec);
      uint8_t binSize = rec.getBinarySizeSecondaryMissionData();
      byte bin[binSize];

      DASSERT(rec.writeBinarySecondaryMissionData(bin, binSize) == binSize);

      clearValues(rec);

      DASSERT(rec.readBinarySecondaryMissionData(bin, binSize) == binSize);

      checkValues(rec);

      DPRINTLN(DBG, "Test successful");
      DPRINTLN(DBG, "Test: read/write binary [END]");
    }
    void testCSVPrint(GMiniRecord &rec) {
      DPRINTLN(DBG, "Test: CSV print [BEGIN]");

      DPRINTLN(DBG, "Printing secondary mission CSV Header without additional separators")
      rec.printCSV_SecondaryMissionHeader(Serial, false, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Header without preceding separator, but with final separator")
      rec.printCSV_SecondaryMissionHeader(Serial, false, true);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Header without final separator, but with preceding separator")
      rec.printCSV_SecondaryMissionHeader(Serial, true, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Header with preceding and final separator")
      rec.printCSV_SecondaryMissionHeader(Serial, true, true);

      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data without additional separators")
      rec.printCSV_SecondaryMissionData(Serial, false, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data without preceding separator, but with final separator")
      rec.printCSV_SecondaryMissionData(Serial, false, true);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data without final separator, but with preceding separator")
      rec.printCSV_SecondaryMissionData(Serial, true, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data with preceding and final separator")
      rec.printCSV_SecondaryMissionData(Serial, true, true);


      DPRINTLN(DBG, "\nNote: Please check for yourself if the test was successful");
      DPRINTLN(DBG, "Test: CSV print [END]");
    }
    void testHumanReadablePrint(GMiniRecord &rec) {
      DPRINTLN(DBG, "Test: Human readable print [BEGIN]");
      rec.printSecondaryMissionData(Serial);
      DPRINTLN(DBG, "Note: Please check for yourself if the test was successful");
      DPRINTLN(DBG, "Test: Human readable print [END]");
    }

  private:
    void initValues(GMiniRecord &rec) {
      rec.sourceID = 123;

      rec.subCanEjected = true;
    }
    void clearValues(GMiniRecord &rec) {
      rec.clearSecondaryMissionData();
    }
    void checkValues(GMiniRecord &rec) {
      DASSERT(rec.sourceID == 123);
      DASSERT(rec.subCanEjected == true);


    }
    void checkClearedValues(GMiniRecord &rec) {
      DASSERT(rec.subCanEjected == 0);
      DASSERT(rec.sourceID == 0);

      DASSERT(rec.subCanEjected == 0);
    }
} ;

void setup() {
  DINIT(115200);

  GMiniRecord rec;
  GMiniRecord_Test test;

  test.testClear(rec);
  test.testCSVLen(rec);
  test.testReadWriteBinary(rec);
  test.testCSVPrint(rec);
  test.testHumanReadablePrint(rec);

  DPRINTLN(DBG, "Testing procedure complete");
}

void loop() {
  // put your main code here, to run repeatedly:

}
