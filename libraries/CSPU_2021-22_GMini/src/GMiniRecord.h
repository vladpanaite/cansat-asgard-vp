#pragma once
#include "CansatRecord.h"

/** @ingroup GMiniCSPU
    @brief The record carrying all data acquired or computed by the G-Mini CanSat
*/
class GMiniRecord : public CansatRecord {
  public:
    uint8_t sourceID; /**< A number identifying the source of the data: 0=main can, 1,2, etc= subcans */
    bool subCanEjected; /**< True if the subcans are ejected, false otherwise */

  protected:
    virtual void printCSV_SecondaryMissionData(Stream &str, bool startWithSeparator, bool finalSeparator) const;
    virtual void printCSV_SecondaryMissionHeader(Stream &str, bool startWithSeparator, bool finalSeparator) const;
    virtual void clearSecondaryMissionData();
    virtual uint16_t getSecondaryMissionMaxCSV_Size() const;
    virtual uint16_t getSecondaryMissionCSV_HeaderSize() const;
    virtual void printSecondaryMissionData(Stream& str) const;
    virtual uint8_t writeBinarySecondaryMissionData(uint8_t* const destinationBuffer, uint8_t bufferSize) const;
    virtual uint8_t readBinarySecondaryMissionData(const uint8_t* const sourceBuffer, const uint8_t bufferSize);
    virtual uint8_t getBinarySizeSecondaryMissionData() const;

    friend class GMiniRecord_Test;
};
