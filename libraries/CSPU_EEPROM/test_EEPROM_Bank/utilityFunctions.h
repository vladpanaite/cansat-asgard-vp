
#pragma once
#include "HardwareScanner.h"
#include "EEPROM_BankWithTools.h"
#include "Arduino.h"
#include "DebugCSPU.h"

typedef struct TestRecord {
  byte aByte;
  int anInt;
  float aFloat;
  unsigned int anUnsignedInt;
  char   aChar;
} TestRecord;

extern TestRecord testData;

void initTestData(byte readCounterValue=0xff, byte writeCounterValue=0xff);

void dumpTestData(TestRecord r);

void printSize(unsigned long size);

unsigned long getExpectedTotalSize(HardwareScanner &hw) ;

void checkSpace(const EEPROM_Bank &bank, unsigned long expectedSize, unsigned long expectedFreeSpace) ;

void doIdle(EEPROM_Bank &bank, byte durationInSeconds);

void storeDataRecords(EEPROM_Bank * bank, int numRecords) ;
void readDataRecords(EEPROM_BankWithTools* bank, int numRecords) ;




