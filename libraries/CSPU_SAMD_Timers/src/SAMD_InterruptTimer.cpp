/*
   SAMD_ISR_Timer.cpp
*/
#ifdef ARDUINO_ARCH_SAMD

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop
// SAMDTimerInterrupt Library includes
// For some reason,some handlers are *defined* in the header files, so they cannot possibly
// be included in more than *one* compilation unit. Hence the definition of a local class for the
// static library-related information.
#include "SAMDTimerInterrupt.h"
#include "SAMD_ISR_Timer.h"

#define DEBUG_CSPU
#define DBG_SAMD_ISR_TIMER 0
#define DBG_SAMD_ISR_TIMER_DIAGNOSTIC 1
#include "DebugCSPU.h"
#include "SAMD_InterruptTimer.h"

// ---- Static helper class
class SAMD_InterruptTimerHelper {
  public:
    SAMD_InterruptTimerHelper() {};
    ~SAMD_InterruptTimerHelper() {
      freeResources();
    };

    int setInterval(uint32_t period, timerCallback f);
    bool changeInterval(unsigned numTimer, unsigned long d) {
      if (!checkResources()) return false;
      return theISR_Timer->changeInterval(numTimer, d);
    }
    void enableAll() {
      // Enabling does not make sense before any timers is activated
      // OR DOES IT? TO BE CHECKED.
      // if (checkResources()) theISR_Timer->enableAll();
      allocateResourcesIfNeeded();
      theISR_Timer->enableAll();
    };
    void disableAll() {
      // if (checkResources()) theISR_Timer->disableAll();
      // DOES THIS WORK ??
      allocateResourcesIfNeeded();
      theISR_Timer->disableAll();
    };
    void enable(unsigned numTimer) {
      if (checkResources()) theISR_Timer->enable(numTimer);
    };
    void disable(unsigned numTimer) {
      if (checkResources()) theISR_Timer->disable(numTimer);
    };
    void deleteTimer(unsigned numTimer) {
      if (checkResources()) {
        theISR_Timer->deleteTimer(numTimer);
        releaseUnusedResources();
      }
    };
    bool isEnabled(unsigned numTimer) {
      if (checkResources()) return theISR_Timer->isEnabled(numTimer);
      else return false;
    };
    void toggle(unsigned numTimer) {
      if (checkResources()) theISR_Timer->toggle(numTimer);
    };
    unsigned getNumTimers() {
      unsigned num = 0;
      if (theISR_Timer) {
        num = theISR_Timer->getNumTimers();
        DPRINTSLN(DBG_SAMD_ISR_TIMER, "theISR_Timer ok");
      }
      return num;
    };
    unsigned getNumAvailableTimers() {
      if (!theISR_Timer) {
        return 16;
      }
      else return theISR_Timer->getNumAvailableTimers();
    };
  private:
    /** Initialize the underlying hardware timer */
    bool initHW_Timer();
    /** Initialize the underlying hardware timer, if not already done. */
    bool allocateResourcesIfNeeded() {
      if (theHW_Timer)  {
        DPRINTSLN(DBG_SAMD_ISR_TIMER, "Resources already allocated.");
        return true;
      } else {
        initHW_Timer();
        return checkResources();
      }
    }

    void releaseUnusedResources() {
      if (!theISR_Timer) return;
      if (theISR_Timer->getNumTimers() == 0) {
        DPRINTSLN(DBG_SAMD_ISR_TIMER, "Resources can be freed.");
        freeResources();
      }
    }

    /** IRQ handler for underlying hardware timer */
    static void TimerHandler();
    /** Free all underlying resources */
    void freeResources();
    /** Check whether resources are allocated */
    bool checkResources();

    static SAMDTimer* theHW_Timer;   /** The underlying hardware timer object */
    static SAMD_ISR_Timer* theISR_Timer; /** The timer object from SAMD_InterruptTimers lib */


    static constexpr bool ReleaseDynamicResourcesWhenNotNeeded = false;
    /**< Set to true to have dynamically allocated objects released when no longer used.
         This could fragment memory. 
         08/2021: The underlying library does not deallocate objects properly (does the destructor
                  actually remove the HW timer?). The board crashed when deallocating resources.
                  keep ReleaseDynamicResourcesWhenNotNeeded to FALSE. */
};

SAMDTimer* SAMD_InterruptTimerHelper::theHW_Timer = nullptr;
SAMD_ISR_Timer* SAMD_InterruptTimerHelper::theISR_Timer = nullptr;
// Set to true to have dynamically allocated objects released when no longer used.
// This could fragment memory.
constexpr bool ReleaseDynamicResourcesWhenNotNeeded = false;

void SAMD_InterruptTimerHelper::freeResources() {
  if (ReleaseDynamicResourcesWhenNotNeeded) {
    if (theISR_Timer) { // Release ISR_Timer first!
      delete theISR_Timer;
      theISR_Timer = nullptr;
      DPRINTSLN(DBG_SAMD_ISR_TIMER, "ISR Timer freed");
    }
    if (theHW_Timer) {
      delete theHW_Timer;
      theHW_Timer = nullptr;
      DPRINTSLN(DBG_SAMD_ISR_TIMER, "HW timer freed");
    }
    DPRINTSLN(DBG_SAMD_ISR_TIMER, "Resources freed");
  } else {
    DPRINTSLN(DBG_SAMD_ISR_TIMER, "Resources NOT freed (ReleaseDynamicResourcesWhenNotNeeded=0)");
  }
}

bool SAMD_InterruptTimerHelper::checkResources() {
  if ((!theHW_Timer)  || (!theISR_Timer)) {
    DPRINTSLN(DBG_SAMD_ISR_TIMER_DIAGNOSTIC, "Error: resources not allocated!");
    return false;
  } else {
    DPRINTSLN(DBG_SAMD_ISR_TIMER, "Resources checked!");
    return true;
  }
}

void SAMD_InterruptTimerHelper::TimerHandler() {
  theISR_Timer->run();
}

bool SAMD_InterruptTimerHelper::initHW_Timer() {
  theHW_Timer = new SAMDTimer(TIMER_TC3); // Only TC3 is possible on both SAMD21 and SAMD51
  if (theHW_Timer) {
    bool ok = theHW_Timer->attachInterruptInterval(1000UL, SAMD_InterruptTimerHelper::TimerHandler);
    if (ok) {
      DPRINTSLN(DBG_SAMD_ISR_TIMER, "HW Timer set");
    } else {
      DPRINTSLN(DBG_SAMD_ISR_TIMER_DIAGNOSTIC, "Cannot set HW Timer");
      return false;
    }
  } else {
    DPRINTSLN(DBG_SAMD_ISR_TIMER_DIAGNOSTIC, "Could not allocate SAMD_Timer object");
    return false;
  }
  theISR_Timer = new SAMD_ISR_Timer;
  if (theISR_Timer) {
    DPRINTSLN(DBG_SAMD_ISR_TIMER, "ISR_Timer created");
  } else {
    DPRINTSLN(DBG_SAMD_ISR_TIMER_DIAGNOSTIC, "Could not allocate SAMD_ISR_Timer object");
    return false;
  }
  DPRINTS(DBG_SAMD_ISR_TIMER, "Initialization ok. ");
  DPRINTSLN(DBG_SAMD_ISR_TIMER, SAMD_TIMER_INTERRUPT_VERSION);
  return true;
}


int SAMD_InterruptTimerHelper::setInterval(uint32_t period, timerCallback f) {
  allocateResourcesIfNeeded();
  checkResources();
  int result = theISR_Timer->setInterval(period, f);
  if (result == -1) {
    DPRINTSLN(DBG_SAMD_ISR_TIMER_DIAGNOSTIC, "Could not set interval");
  } else {
    DPRINTS(DBG_SAMD_ISR_TIMER, "Interval (msec) set to ");
    DPRINTLN(DBG_SAMD_ISR_TIMER, period);
  }
  return result;
}

static SAMD_InterruptTimerHelper theHelper;

// -------------------------------------------------------------------------
SAMD_InterruptTimer::SAMD_InterruptTimer() : libID(-1) {}

SAMD_InterruptTimer::~SAMD_InterruptTimer() {
  clear();
}

bool SAMD_InterruptTimer::start(uint32_t period, timerCallback f) {
  if (libID >= 0) {
    DPRINTSLN(DBG_SAMD_ISR_TIMER_DIAGNOSTIC, "Error: timer already started.");
    return false;
  }

  int result = theHelper.setInterval(period, f);
  if (result != -1) {
    libID = result;
    return true;
  } else {
    DPRINTSLN(DBG_SAMD_ISR_TIMER, "Timer started!");
    return false;
  }
}

bool SAMD_InterruptTimer::changePeriod(uint32_t period) {
  if (libID < 0) return false;
  return theHelper.changeInterval(libID, period);
}

bool SAMD_InterruptTimer::isEnabled() {
  if (libID < 0) return false;
  return theHelper.isEnabled(libID);
}

void SAMD_InterruptTimer::enable() {
  if (libID < 0) return;
  theHelper.enable(libID);
}

void SAMD_InterruptTimer::disable() {
  if (libID < 0) return;
  theHelper.disable(libID);
}

void SAMD_InterruptTimer::enableAll() {
  theHelper.enableAll();
}
void SAMD_InterruptTimer::disableAll() {
  theHelper.disableAll();
}
void SAMD_InterruptTimer::toggle() {
  if (libID < 0) return;
  theHelper.toggle(libID);
}

void SAMD_InterruptTimer::clear() {
  if (libID < 0) return;
  theHelper.deleteTimer(libID);
  libID = -1;
}

unsigned SAMD_InterruptTimer::getNumTimers() {
  return theHelper.getNumTimers();
}
unsigned SAMD_InterruptTimer::getNumAvailableTimers() {
  return theHelper.getNumAvailableTimers();
}
#endif // ARDUINO_ARCH_SAMD
