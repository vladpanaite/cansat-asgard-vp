/*
    IsatisStorageManager.cpp

    Created on: 20 janv. 2018
*/

#include "IsatisStorageManager.h"
#include "IsatisConfig.h"
#include "IsatisAcquisitionProcess.h"
#include "StringStream.h"

IsatisStorageManager::IsatisStorageManager(unsigned int theCampainDurationInSec, unsigned int theMinStoragePeriodInMsec)
  : StorageManager(sizeof(IsatisDataRecord), theCampainDurationInSec, theMinStoragePeriodInMsec) {

}

void IsatisStorageManager::storeIsatisRecord(const IsatisDataRecord& data, const bool useEEPROM)
{
  DPRINTSLN(DBG_STORAGE, "IsatisStorageManager::storeOneRecord");
  String CSV_Version;
  CSV_Version.reserve(50);
  StringStream sstr(CSV_Version);
  data.printCSV(sstr);
  const byte * binaryData= (const byte*) &data;
  storeOneRecord( binaryData, CSV_Version, useEEPROM);
}

