#include "GPY.h"
#include "IsatisConfig.h"
#include "Timer.h"

/*
    Read the GP2 sensor output voltage
*/

double GPY::getOutputV(const byte dataOutPinNbr, const byte LED_VccPinNbr) {
  DBG_TIMER("GPY::getOutputV");
  digitalWrite(LED_VccPinNbr, LOW);
  delayMicroseconds(GPY_DelayBeforeSamplingInMicroSec);
  double analogOutput = analogRead(dataOutPinNbr);
  delayMicroseconds(GPY_DelayAfterSamplingInMicroSec);
  digitalWrite(LED_VccPinNbr, HIGH);
  delayMicroseconds(GPY_DelayLED_OffInMicroSec);
  //Arduino analog read value range is 0 ~ 1023, the following conversion is 0 ~ 5v
  double outputV = analogOutput / 1024 * 5;
  return outputV;
}

// Calculate average
double GPY::getOutputV_Average(   const byte dataOutPinNbr, 
                                  const byte LED_VccPinNbr, 
                                  const byte numSamples, 
                                  const double maxDeltaInSet, 
                                  const bool repeatBadQualityReadings,
                                  bool &qualityGood) {
  DBG_TIMER("GPY::GetOutputV_Average");
  double sum;
  double minV, maxV;
  double newReading;
  do {
    minV = 10;
    maxV = 0;
    sum=0;
    for (byte sampleCount = 1; sampleCount <= numSamples; sampleCount++)
    {
      newReading =  getOutputV(dataOutPinNbr, LED_VccPinNbr);
      if (newReading < minV) minV = newReading;
      if (newReading > maxV) maxV = newReading;
      sum += newReading;
      DPRINTS(DBG_GETOUTPUTV_AVERAGE, "$");
      DPRINT(DBG_GETOUTPUTV_AVERAGE, newReading);
    }

    if ((maxV - minV) > maxDeltaInSet) {
      // Aberrant reading somewhere
      DPRINTS(DBG_GETOUTPUTV_AVERAGE, " Quality not ok: min=");
      DPRINT(DBG_GETOUTPUTV_AVERAGE, minV);
      DPRINTS(DBG_GETOUTPUTV_AVERAGE, ", max=");
      DPRINT(DBG_GETOUTPUTV_AVERAGE, maxV);
      DPRINTS(DBG_GETOUTPUTV_AVERAGE, ", delta=");
      DPRINT(DBG_GETOUTPUTV_AVERAGE, maxV-minV);
      DPRINTS(DBG_GETOUTPUTV_AVERAGE, ", average=");
      DPRINTLN(DBG_GETOUTPUTV_AVERAGE, sum/numSamples);
      qualityGood = false;
    }
    else {
      // Noise with acceptable limit.
      qualityGood = true;
      DPRINTSLN(DBG_GETOUTPUTV_AVERAGE, " Quality ok");
    }
    // No delays here: the delay is included in getOutputV.
  } while (repeatBadQualityReadings && !qualityGood);
  return (sum / numSamples);
}

// Turn fan on or off
void GPY::setFanOn(bool status) {
  if (status == true) {
    digitalWrite(Fan_DigitalPin, HIGH);
  }
  else {
    digitalWrite(Fan_DigitalPin, LOW);
  }

}



