/*
   IsatisHW_Scanner.cpp

    Created on: 26 janv. 2018
*/

#include "IsatisConfig.h"
#include "IsatisHW_Scanner.h"
#include "SdFat.h"
#include "GPY.h"
#include "ADT7420.h"

IsatisHW_Scanner::IsatisHW_Scanner(const byte unusedAnalogInPin) : HardwareScanner(unusedAnalogInPin) {
  flags.SD_CardReaderAvailable = false;
  flags.BMP_SensorAvailable = false;
  flags.ADT_SensorAvailable = false;
}

void IsatisHW_Scanner::IsatisInit() {
  HardwareScanner::init(I2C_lowestAddress, I2C_highestAddress, 1, 400);
  pinMode(GPY_dataOutPinNbr, INPUT);  // Defined as input (ADC reads 1 analog)
  pinMode(GPY_LED_VccPinNbr, OUTPUT); // Defined as output
  digitalWrite(GPY_LED_VccPinNbr, HIGH);  // By default, turn LED off.
  pinMode(Fan_DigitalPin, OUTPUT);    // Defined as output
  GPY::setFanOn(false);                    // Turn fan off by default

  flags.BMP_SensorAvailable = isI2C_SlaveUpAndRunning( I2C_BMP_SensorAddress);
  flags.ADT_SensorAvailable = isI2C_SlaveUpAndRunning( I2C_ADT_SensorAddress);
  // No way to detect the BPY sensor. We can only assume it is there and read garbage if it is not.

  HardwareSerial* RF = getRF_SerialObject();
  if (isSerialPortAvailable(1)) {
    RF->begin(serial1BaudRate);
    while (!RF) ;
    DPRINTSLN(DBG_DIAGNOSTIC, "RF Serial1 init ok");
  }
  else DPRINTSLN(DBG_DIAGNOSTIC, "No Serial1 to initialize");
}

void IsatisHW_Scanner::checkAllSPI_Devices() {
  DPRINTSLN(DBG_CHECK_SPI_DEVICES, "IsatisHW_Scanner::checkAllSPI_Devices");
  // Check for SD Card reader
  SdFat SD;
  pinMode(SD_CardChipSelect, OUTPUT);
  if (SD.begin(SD_CardChipSelect)) {
    flags.SD_CardReaderAvailable = true;
  } else {
    DPRINTSLN(DBG_CHECK_SPI_DEVICES, "Card failed, or not present");
  }
  // do nothing more
}

byte IsatisHW_Scanner::getLED_PinNbr(LED_Type type) {
  byte result;
  switch (type) {
    case Init:
      result = pin_InitializationLED;
      break;
    case Storage:
      result =  pin_StorageLED;
      break;
    case Transmission:
      result =  pin_TransmissionLED;
      break;
    case Acquisition:
      result =  pin_AcquisitionLED;
      break;
    case Heartbeat:
      result =  pin_HeartbeatLED;
      break;
    case Campaign:
      result =  pin_CampaignLED;
      break;
    case UsingEEPROM:
      result =  pin_UsingEEPROM_LED;
      break;
    default:
      DASSERT(false);
      result =  0;
  }
  return result;
}

void IsatisHW_Scanner::printSPI_Diagnostic(Stream& stream) const {
  if (flags.SD_CardReaderAvailable) {
    stream.println(F("SPI bus: SD Card ok."));
  }
  else {
    stream.println(F("SPI bus: no SD Card detected."));
  }
  return;
}

void IsatisHW_Scanner::printI2C_Diagnostic(Stream& stream) const {
  HardwareScanner::printI2C_Diagnostic(stream);
  delay(200);
  if (flags.BMP_SensorAvailable) {
    stream << F("Assuming I2C slave at ") << I2C_BMP_SensorAddress << F(" is the BMP sensor") << ENDL;
  }
  else
  {
    stream << F("Missing BMP sensor at I2C address ") << I2C_BMP_SensorAddress << ENDL;
  }
  if (flags.ADT_SensorAvailable) {
    stream << F("Assuming I2C slave at ") << I2C_ADT_SensorAddress << F(" is the ADT7420 sensor") << ENDL;
  }
  else
  {
    stream << F("Missing ADT sensor at I2C address ") << I2C_ADT_SensorAddress << ENDL;
  }
  stream << F("GPY sensor presence cannot be confirmed") << ENDL;
}

#ifdef IGNORE_EEPROMS
byte IsatisHW_Scanner::getNumExternalEEPROM() const {
  Serial << F("*** EEPROMS ignored for test ***") << ENDL;
  return 0;
}
#endif




