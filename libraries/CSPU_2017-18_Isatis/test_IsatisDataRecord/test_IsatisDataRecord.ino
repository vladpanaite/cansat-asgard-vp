
/* 
 *  Test for IsatisDataRecord methods
 */

#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "IsatisDataRecord.h"
#include "StringStream.h"

IsatisDataRecord record;

void setup() {
  DINIT(9600);

  record.BMP_Temperature= 1.23456;
  record.BMP_Pressure=2.34567;
  record.BMP_Altitude=3.45678;
  record.GPY_OutputV= 4.56789;
  record.startTimestamp= 12345;
  record.endTimestamp=98765;
#ifndef USE_MINIMUM_DATARECORD
  record.GPY_DustDensity=9.87654;
  record.GPY_AQI=8.76543;
#endif

  record.print(Serial);
  Serial  << ENDL << F("CSV string: ") << ENDL;
  record.printCSV_Header(Serial);
  Serial << ENDL;
  record.printCSV(Serial);
  Serial << ENDL; 

  Serial << ENDL << "Printing into a String:" << ENDL;
  String str;
  StringStream sstr(str);
  record.printCSV(sstr);
  Serial << "Resulting string (CSV):" << ENDL << str << ENDL << ENDL;

  record.clear();
  Serial << F("Cleared record:") << ENDL;
  record.print(Serial);
  Serial << ENDL;
  record.printCSV(Serial);
  Serial << ENDL << F("End of test") << ENDL; 
}

void loop() {
  // put your main code here, to run repeatedly:

}
