/*
 * ThermistorGT103J1K.h
 */

#pragma once

#include "Thermistor.h"

/** @ingroup CSPU_Thermistors
  @brief A GT103J1K thermistance.
  Use this class to read thermistor resistance and convert to degrees.
  Wiring: VCC to thermistor, thermistor to serialresistor, serialresistor to ground.
*/
class ThermistorGT103J1K: public Thermistor {
public:
	/**
	   * @param theVcc the source level (in volts)
	   * @param theAnalogPinNbr this is the number of the analog pin
	   * @param theSerialResistor this is the value of the serial resistor, in ohms.
	   */
	ThermistorGT103J1K(const float theVcc,
             const uint8_t theAnalogPinNbr,
            const float theSerialResistor)
	: Thermistor(theVcc, theAnalogPinNbr, theSerialResistor) {};
	virtual ~ThermistorGT103J1K(){};

    /** Obtain the temperature by reading the voltage (averaging several readings),
     *  deriving the resistance of the thermistor and then the corresponding temperature.
     *  @return The resulting temperature in °C.
     */
    virtual float readTemperature()const ;
};
