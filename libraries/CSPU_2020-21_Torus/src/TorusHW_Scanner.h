/*
 * TorusHW_Scanner.h
 */

#pragma once
#include "CansatHW_Scanner.h"
#include "TorusServoWinch.h"
#include "TorusConfig.h"

/** @ingroup TorusCSPU
 *  @brief The class in charge of the can hardware in the Torus project.
 *  It derives from CansatHW_Scanner and provides the servowinch-related
 *  features
 */
class TorusHW_Scanner: public CansatHW_Scanner {
public:
	/** @param unusedAnalogInPin The number of an unused analog input pin. It is used
	 *         to read a random seed.
	 */
	TorusHW_Scanner(const byte anUnusedAnalogInPin = unusedAnalogInPinNumber);
	virtual ~TorusHW_Scanner() {};

	/** Overridden
	 * Call once, before using, once the setup() function of the sketch has been entered
	 (to ensure all board functions are up and running)
	 Note that this call takes care of:
	 - calling Wire.begin(), and setting the clock rate to the provided frequency
	 - Configuring the digital pin used for the SD-Card reader, if any.
	 - Checking the presence of an SD-Card reader with provided CS, if any.
	 - initializing a CansatXBeeClient instance (if API mode is active)
	 - initializing the serial ports used for the RF, if any. (but not the GPS!).
	 - checking for the presence of a BMP280 sensor on the I2C
	 - intializing the ServoWinch object.
	 @param SD_CardChipSelect The digital pin used as ChipSelect for the SD-Card reader,
	 or 0 if none used.
	 @param firstI2C_Address  First I2C address to consider.
	 @param firstI2C_Address  First I2C address to consider.
	 @param lastI2C_Address   Last I2C address to consider.
	 @param RF_SerialPort The ID of the serial port to use for the RF transmitter
	 	 	 	 	 	  (0 = none, 1 = Serial1, 2 = Serial2 etc.)
	 @param GPS_SerialPort The ID of the serial port to use for the GPS module
	 	 	 	 	 	   (0 = none, 1 = Serial1, 2 = Serial2 etc.)
	 @param wireLibBitRateInKhz  bitRate to use to initialize the Wire library
	 */
	VIRTUAL
	void init(
			const byte SD_CardCS,
			const byte firstI2C_Address, const byte lastI2C_Address,
			const byte RF_SerialPort, const byte GPS_SerialPort,
			const uint16_t wireLibBitRateInKhz);

	/** Access-method to the servo-winch object
	 * @return A reference to the fully initialized servo-winch object.
	 */
	TorusServoWinch& getServoWinch() { return winch;};
protected:
	/**  @brief Print torus-specific diagnostic information
	 *   @param stream The stream on which the diagnostic must be printed.
	 */
	VIRTUAL void printProjectDiagnostic(Stream& stream) const;
private:
	TorusServoWinch winch;
};

